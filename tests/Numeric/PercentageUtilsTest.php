<?php

namespace App\Tests\Numeric;

use Vanat\SymDocUtils\Numeric\PercentageUtils;
use PHPUnit\Framework\TestCase;

class PercentageUtilsTest extends TestCase
{

    public function testGetPercentageDiff()
    {
        self::assertEquals(-60, PercentageUtils::pctChange(25, 10));
        self::assertEquals(-10, PercentageUtils::pctChange(100, 90));
        self::assertEquals(50, PercentageUtils::pctChange(100,150));
        self::assertEquals(0, PercentageUtils::pctChange(100,100));
    }
}
