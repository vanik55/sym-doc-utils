<?php


namespace App\Tests\Numeric;


use PHPUnit\Framework\TestCase;
use Vanat\SymDocUtils\Numeric\NumericUtils;

class NumericUtilsTest extends TestCase
{

    public function test_sumOfDigits() {
        $result = NumericUtils::sumOfDigits(12345);
        $this->assertEquals(15, $result);
        $result = NumericUtils::sumOfDigits(14597);
        $this->assertEquals(26, $result);
    }

    public function test_isNUmberBetween() {
        self::assertTrue(NumericUtils::isNumberBetween(1, null, null));
        self::assertTrue(NumericUtils::isNumberBetween(1, 1, null));
        self::assertTrue(NumericUtils::isNumberBetween(1, null, 4));
        self::assertTrue(NumericUtils::isNumberBetween(1, 0, 4));
        self::assertFalse(NumericUtils::isNumberBetween(25, null, 12));
        self::assertFalse(NumericUtils::isNumberBetween(25, 1, 12));
        self::assertFalse(NumericUtils::isNumberBetween(25, 111, null));
    }

    public function test_sign() {
        self::assertEquals(1, NumericUtils::sign(54));
        self::assertEquals(1, NumericUtils::sign(0));
        self::assertEquals(-1, NumericUtils::sign(-8));
        self::assertEquals(1, NumericUtils::sign(100));
    }

}
