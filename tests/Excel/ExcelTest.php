<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 08.01.2019
 * Time: 13:06
 */

namespace App\Tests\Excel;


use PHPUnit\Framework\TestCase;
use Vanat\SymDocUtils\Excel\SequentialExcelBuilder;

class ExcelTest extends TestCase
{

    public function test_creteExcel() {

        $excelBuilder = new SequentialExcelBuilder(array("Sloupec 1", "Sloupec 2", "Sloupec 3"));
        $excelBuilder->nextRow();
        $excelBuilder->writeHeader(["A","Další","Hlavička"]);
        $excelBuilder->writeString("Ahoj")->writeString("Dlouhý Dlouhý Dlouhý Dlouhý Dlouhý Dlouhý Dlouhý")
            ->writeNumeric(25.14)->writeDate(new \DateTime())->nextRow()->writeString("Další")->writeString("Krátký")->writeNumeric(null)
            ->nextRow()->writeString("Jedna\nDvě\nTři\nČtyři", true);
        $tmpFileName = $excelBuilder->createFile();
        print_r($tmpFileName);
        file_put_contents("C:/Development/pokus.xlsx", file_get_contents($tmpFileName));
    }

}