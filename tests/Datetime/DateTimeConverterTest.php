<?php

namespace App\Tests\Datetime;

use Vanat\SymDocUtils\Datetime\DateTimeConverter;
use PHPUnit\Framework\TestCase;

class DateTimeConverterTest extends TestCase
{

    public function testMutableFromInterface()
    {
        $dateTime = new \DateTime();
        $result = DateTimeConverter::mutableFromInterface($dateTime);
        self::assertEquals($dateTime->getTimestamp(), $result->getTimestamp());
    }
}
