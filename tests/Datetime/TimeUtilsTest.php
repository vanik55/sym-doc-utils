<?php


namespace App\Tests\Datetime;


use PHPUnit\Framework\TestCase;
use Vanat\SymDocUtils\Datetime\TimeUtils;

class TimeUtilsTest extends TestCase
{

    public function test_isTimeBetween() {
        self::assertTrue(TimeUtils::isTimeBetween("08:00", "09:00", new \DateTime("1974-01-05 08:42:41")));
        self::assertTrue(TimeUtils::isTimeBetween("08:00", null, new \DateTime("1974-01-05 08:42:41")));
        self::assertTrue(TimeUtils::isTimeBetween(null, "18:00", new \DateTime("1974-01-05 08:42:41")));
        self::assertTrue(TimeUtils::isTimeBetween(null, null, new \DateTime("1974-01-05 08:42:41")));
        self::assertFalse(TimeUtils::isTimeBetween("08:00", "09:00", new \DateTime("1974-01-05 18:42:41")));
        self::assertFalse(TimeUtils::isTimeBetween(null, "09:00", new \DateTime("1974-01-05 18:42:41")));
    }

    public function test_secondsToTime() {
        self::assertEquals("01:30" , TimeUtils::secondsToTime(90));
        self::assertEquals("00:05" , TimeUtils::secondsToTime(5));
        self::assertEquals("01:01:00" , TimeUtils::secondsToTime(3660));
        self::assertEquals("25:00:00" , TimeUtils::secondsToTime(3600 * 24 + 3600));
    }

    public function test_minutesToTime() {
        self::assertEquals("01:30" , TimeUtils::minutesToTime(90));
        self::assertEquals("00:05" , TimeUtils::minutesToTime(5));
        self::assertEquals("24:01" , TimeUtils::minutesToTime(24*60 + 1));
        self::assertEquals("25:00" , TimeUtils::minutesToTime(25*60));
    }

}