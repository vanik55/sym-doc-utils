<?php

namespace App\Tests\Datetime;

use Vanat\SymDocUtils\Datetime\DateIntervalUtils;
use PHPUnit\Framework\TestCase;

class DateIntervalUtilsTest extends TestCase
{

    public function testEmptyInterval()
    {
        dump(DateIntervalUtils::emptyInterval());
    }

    public function testParse()
    {
        dump(DateIntervalUtils::parseTime("1:00:00"));
    }

    public function test_createPeriods() {
        $periods = (DateIntervalUtils::createPeriods(new \DateTime("2023-11-01"), new \DateTime("2023-11-30"), 0b1110101));
        foreach ($periods as $period) {
            dump($period->getStartDate()->format("Y-m-d") . " - " . $period->getEndDate()->format("Y-m-d"));
        }
    }
}
