<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 11.12.2018
 * Time: 10:29
 */

namespace App\Tests\Datetime;


use PhpOffice\PhpSpreadsheet\Calculation\DateTime;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use Vanat\SymDocUtils\Datetime\DatetimeUtils;

class DatetimeUtilsTest extends TestCase
{

    public function test_jsStringToMysqlString() {
        $baseString = "2018-12-11T08:34:12.643Z";
        $expectedString = "2018-12-11 08:34:12";
        $this->assertEquals($expectedString, DatetimeUtils::convertTemporalStringToDbString($baseString));
    }

    public function test_jsStringOnlyDateToMysqlString() {
        $baseString = "2018-12-11";
        $expectedString = "2018-12-11 00:00:00";
        $this->assertEquals($expectedString, DatetimeUtils::convertTemporalStringToDbString($baseString));
    }

    public function test_getFirstDayOfMonthNow() {
        $date = DatetimeUtils::getFirstDayOfMonth();
        self::assertEquals("2019-01-01", $date->format("Y-m-d"));
    }

    public function test_getFirstDayOfMonthArbitraryMonth() {
        $date = DatetimeUtils::getFirstDayOfMonth(new \DateTime("2018-09-05 15:15:14"));
        self::assertEquals("2018-09-01 00:00:00", $date->format("Y-m-d H:i:s"));
    }

    public function test_getLastDayOfMonthNow() {
        $date = DatetimeUtils::getLastDayOfMonth();
        self::assertEquals("2019-01-31 00:00:00", $date->format("Y-m-d H:i:s"));
    }

    public function test_getLastDayOfMonthArbitraryMonth() {
        $date = DatetimeUtils::getLastDayOfMonth(new \DateTime("2018-09-05 15:15:14"));
        self::assertEquals("2018-09-30 00:00:00", $date->format("Y-m-d H:i:s"));
    }

    public function test_getFirstDateBetween() {
        $result = DatetimeUtils::getFirstDaysOfMonthsBetweenDates(new \DateTime("2018-10-01"), new \DateTime("2019-01-31"));
        print_r($result);
        self::assertEquals(4, count($result));
    }

    public function test_translateDayName() {
        echo DatetimeUtils::translateDateToName(new \DateTime('2019-08-15'), true);
    }

    public function test_getPeriod() {
        var_dump(DatetimeUtils::createPeriod(new \DateTime("2019-08-01"), new \DateTime("2019-08-31")));
    }

    public function test_createDatetime() {
        dump(DatetimeUtils::createDateTime());
        dump(DatetimeUtils::createDateTime("-15 minute"));
        dump(DatetimeUtils::createDateTime("asdasdsa"));
    }

    public function test_parseDateTime() {
        dump(DatetimeUtils::parseDateTime("2020-07-12T00:00:00"));
    }

    public function test_isDateTimeBetween() {
        self::assertTrue(DatetimeUtils::isDateTimeBetween(new \DateTime(), null, null));
        self::assertTrue(DatetimeUtils::isDateTimeBetween(new \DateTime("2020-10-27"), new \DateTime("2020-01-01"), null));
        self::assertTrue(DatetimeUtils::isDateTimeBetween(new \DateTime("2020-10-27"), null, new \DateTime("2020-12-01")));
        self::assertTrue(DatetimeUtils::isDateTimeBetween(new \DateTime("2020-10-27"), new \DateTime("2020-01-01"), new \DateTime("2020-12-01")));
        self::assertFalse(DatetimeUtils::isDateTimeBetween(new \DateTime("2021-10-27"), new \DateTime("2020-01-01"), new \DateTime("2020-12-01")));
    }

    public function test_add() {
        self::assertEquals(new \DateTime("2020-10-30"), DatetimeUtils::add(new \DateTime("midnight"), 1));
        self::assertEquals(new \DateTime("2020-10-28"), DatetimeUtils::add(new \DateTime("midnight"), -1));
    }

    public function test_getDayNum() {
        self::assertEquals(0, DatetimeUtils::getDayNumber(new \DateTime("2020-11-02"),0));
        self::assertEquals(2, DatetimeUtils::getDayNumber(new \DateTime("2020-11-04"),0));
        self::assertEquals(3, DatetimeUtils::getDayNumber(new \DateTime("2020-11-04"),1));
    }

    public function test_generateTimesBetween() {
        $dates = DatetimeUtils::generateTimesBetween(new \DateTime("2021-03-17 00:00:00"), new \DateTime("2021-03-27 00:00:00"), 1, "day");
        self::assertEquals(11, count($dates));
        var_dump($dates);
    }

    public function test_mergeDateAndTime() {
        $date = DatetimeUtils::mergeDateAndTime(new \DateTime("2020-01-01"), "15:00");
        self::assertEquals(new \DateTime("2020-01-01 15:00:00"), $date);
    }

    public function test_minutesBetween() {
        $dateTime1 = new \DateTime("2021-05-28 14:15");
        $dateTime2 = new \DateTime("2021-05-28 14:45");
        self::assertEquals(30, DatetimeUtils::getMinutesBetween($dateTime1, $dateTime2));
        self::assertEquals(-30, DatetimeUtils::getMinutesBetween($dateTime2, $dateTime1));
    }

    public function test_dateArrayToInClause() {
        $testDates = [new \DateTime("2021-06-01"), new \DateTime("2021-06-02")];
        $result = DatetimeUtils::dateArrayToInClause($testDates);
        self::assertEquals("2021-06-01", $result[0]);
        self::assertEquals("2021-06-02", $result[1]);
    }

    public function test_getDaysShortcutsFromBitmap() {
        $result = DatetimeUtils::getDaysShortcutsFromBitmap(0b1111111);
        self::assertEquals(["Po", "Út", "St", "Čt", "Pá", "So", "Ne"], $result);
        $result = DatetimeUtils::getDaysShortcutsFromBitmap(0b1000001);
        self::assertEquals(["Po", "Ne"], $result);
    }

}
