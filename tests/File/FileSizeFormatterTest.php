<?php

namespace App\Tests\File;

use Vanat\SymDocUtils\File\FileSizeFormatter;
use PHPUnit\Framework\TestCase;

class FileSizeFormatterTest extends TestCase
{

    public function testFormatBytes()
    {
        echo FileSizeFormatter::formatBytes(565165651).PHP_EOL;
        echo FileSizeFormatter::formatBytes(565165651,1).PHP_EOL;
        echo FileSizeFormatter::formatBytes(565165651, 0).PHP_EOL;
        echo FileSizeFormatter::formatBytes(5651656516546).PHP_EOL;
        echo FileSizeFormatter::formatBytes(565165).PHP_EOL;
        echo FileSizeFormatter::formatBytes(651).PHP_EOL;
    }
}
