<?php

namespace App\Tests\Money;

use Vanat\SymDocUtils\Money\MoneyUtils;
use PHPUnit\Framework\TestCase;

class MoneyUtilsTest extends TestCase
{

    public function testAddVatToPrice()
    {
        $result = MoneyUtils::addVatToPrice(MoneyUtils::of(83), 21);
        self::assertEquals(100.43, $result->getAmount()->toFloat());
    }

    public function testVatFromNoVatPrice() {
        $result = MoneyUtils::getVatFromBasePrice(MoneyUtils::of(100), 21);
        self::assertEquals(21, $result->getAmount()->toFloat());
    }
}
