<?php

namespace App\Tests\Money;

use Vanat\SymDocUtils\Money\MoneyDiscount;
use PHPUnit\Framework\TestCase;
use Vanat\SymDocUtils\Money\MoneyUtils;

class MoneyDiscountTest extends TestCase
{

    public function testApplyDiscount()
    {
        $result = MoneyDiscount::applyDiscount(MoneyUtils::of(100), 10);
        self::assertEquals(MoneyUtils::of(90), $result);

        $result = MoneyDiscount::applyDiscount(MoneyUtils::of(500), 10);
        self::assertEquals(MoneyUtils::of(450), $result);

        $result = MoneyDiscount::applyDiscount(MoneyUtils::of(15), 10);
        self::assertEquals(MoneyUtils::of(13.5), $result);
    }

    public function testApplyMultipleDiscounts() {
        $result = MoneyDiscount::applyMultipleDiscounts(MoneyUtils::of(100), [10,10]);
        self::assertEquals(MoneyUtils::of(81), $result->getAmount());
        self::assertEquals(19, $result->getTotalDiscount());

        $result = MoneyDiscount::applyMultipleDiscounts(MoneyUtils::of(100), [10,15]);
        self::assertEquals(MoneyUtils::of(76.5), $result->getAmount());
        self::assertEquals(23.5, $result->getTotalDiscount());

        $result = MoneyDiscount::applyMultipleDiscounts(MoneyUtils::of(100), [0,null]);
        self::assertEquals(MoneyUtils::of(100), $result->getAmount());
        self::assertEquals(null, $result->getTotalDiscount());
    }

    public function testApplyMultipleDiscounts2() {
        $result = MoneyDiscount::applyMultipleDiscounts(MoneyUtils::of(60), [15,10]);
        $result = MoneyDiscount::applyMultipleDiscounts($result->getAmount(), [10]);
        dump($result);
    }

    public function testGetTotalDiscount() {
        $result = MoneyDiscount::getTotalDiscountPct([15,10]);
        dump($result);
    }
}
