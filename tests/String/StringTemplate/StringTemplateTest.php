<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 12.03.2019
 * Time: 12:42
 */

namespace App\Tests\String\StringTemplate;


use PHPUnit\Framework\TestCase;
use Vanat\SymDocUtils\String\StringTemplate\ColonParameterStringEngine;
use Vanat\SymDocUtils\String\StringTemplate\Engine;

class StringTemplateTest extends TestCase
{

    public function test_composeString() {
        $engine = new Engine();
        $result = $engine->render("My name is {name} {surname}", array("name" => "Tomáš", "surname" => "Váňa"));
        $this->assertEquals("My name is Tomáš Váňa", $result);
    }

    public function test_composeStringDifferent() {
        $engine = new Engine(':', '');
        $result = $engine->render("My name is :name :surname", array("name" => "Tomáš", "surname" => "Váňa"));
        $this->assertEquals("My name is Tomáš Váňa", $result);
    }

    public function test_composeStringColonEngine() {
        $engine = new ColonParameterStringEngine();
        $result = $engine->render("My name is :name :surname", array("name" => "Tomáš", "surname" => "Váňa"));
        $this->assertEquals("My name is Tomáš Váňa", $result);
    }

}