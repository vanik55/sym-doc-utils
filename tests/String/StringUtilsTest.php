<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 20.12.2018
 * Time: 15:26
 */

namespace App\Tests\String;


use phpDocumentor\Reflection\Types\Self_;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use Vanat\SymDocUtils\String\StringUtils;

class StringUtilsTest extends TestCase
{

    public function test_getInitials()
    {
        $this->assertEquals("TV", StringUtils::getInitials("Tomáš","Váňa"));
        $this->assertEquals("TV", StringUtils::getInitials("tomáš","váňa"));
        $this->assertEquals("T", StringUtils::getInitials("tomáš",null));
        $this->assertEquals("V", StringUtils::getInitials(null,"váňa"));
        $this->assertEquals("", StringUtils::getInitials(null, null));
    }

    public function test_StartWith() {
        self::assertTrue(StringUtils::startsWith("ABC", "A"));
        self::assertTrue(StringUtils::startsWith("checkin.hotelhertz.cz", "checkin."));
    }

    public function test_RemoveDiacritics() {
        $result = StringUtils::removeDiacritics("Váňa.Tomáš");
        self::assertEquals("Vana.Tomas", $result);
    }

    public function test_getInitialsDiacritics() {
        echo StringUtils::getInitials("David", "Žák");
    }

    public function test_removeWhitespace() {
        self::assertEquals("25656112", StringUtils::removeAllWhitespace("	25656112    "));
        self::assertEquals("CZ25656112", StringUtils::removeAllWhitespace("CZ	25656112        "));
    }

    public function test_coalesce() {
        self::assertEquals("ABC", StringUtils::coalesce("", NULL, "ABC"));
    }
}
