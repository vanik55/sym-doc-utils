<?php


namespace App\Tests\Report;


use PHPUnit\Framework\TestCase;
use Vanat\SymDocUtils\Report\PivotedReportUtils;

class PivotedReportUtilsTest extends TestCase
{

    public function test_getPivotColumnsForAllReport() {
        $defaultRowValue = 0;
        $result = PivotedReportUtils::generateDateTimedReportColumns(new \DateTime(), new \DateTime(), "ALL", $defaultRowValue);
        dump($result);
        self::assertEquals("Celkem",$result->getColumns()[0]);
        self::assertEquals($defaultRowValue,$result->getDefaultRow()[0]);
    }

    public function test_getPivotColumnsDay() {
        $defaultRowValue = 0;
        $result = PivotedReportUtils::generateDateTimedReportColumns(new \DateTime("2020-02-20"),
            new \DateTime("2020-02-28"), "DAY", $defaultRowValue);
        //dump($result);
        echo json_encode($result->getColumns()).PHP_EOL;
        echo json_encode($result->getDefaultRow());
        self::assertEquals(9, count($result->getColumns()));
        //self::assertArrayHasKey("2020-02-28",$result->getDefaultRow());
        dump($result->mergeRow([["2020-02-23" => 5], ["2020-02-25" => 6]]));
    }

    public function test_getPivotColumnsMonth() {
        $defaultRowValue = 0;
        $result = PivotedReportUtils::generateDateTimedReportColumns(new \DateTime("2019-01-01"),
            new \DateTime("2020-12-01"), "MONTH", $defaultRowValue);
        dump($result);
    }

    public function test_getPivotColumnsYear() {
        $defaultRowValue = 0;
        $result = PivotedReportUtils::generateDateTimedReportColumns(new \DateTime("2010-01-01"), new \DateTime("2020-12-01"), "YEAR", $defaultRowValue);
        dump($result);
    }

    public function test_getDayInterval() {
        $result = PivotedReportUtils::calculateReportsDates("DAY", "2020-01-01", "2020-02-02");
        self::assertEquals(new \DateTime("2020-01-01"), $result->getFrom());
        self::assertEquals(new \DateTime("2020-02-02"), $result->getTill());
    }

    public function test_getMonthInterval() {
        $result = PivotedReportUtils::calculateReportsDates("MONTH", "2020-01", "2020-02");
        self::assertEquals(new \DateTime("2020-01-01"), $result->getFrom());
        self::assertEquals(new \DateTime("2020-02-29"), $result->getTill());
    }

    public function test_getYearInterval() {
        $result = PivotedReportUtils::calculateReportsDates("YEAR", "2020", "2021");
        self::assertEquals(new \DateTime("2020-01-01"), $result->getFrom());
        self::assertEquals(new \DateTime("2021-12-31"), $result->getTill());
    }

}