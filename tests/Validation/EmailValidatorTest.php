<?php


namespace App\Tests\Validation;


use PHPUnit\Framework\TestCase;
use Vanat\SymDocUtils\Validation\EmailValidator;

class EmailValidatorTest extends TestCase
{

    public function test_validateEmail() {
        self::assertTrue(EmailValidator::isEmailValid("vanik55@email.cz"));
        self::assertTrue(EmailValidator::isEmailValid("tom.vanik55@email.cz"));
        self::assertFalse(EmailValidator::isEmailValid("vanik55email.cz"));
    }

}