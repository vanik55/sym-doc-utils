<?php

namespace App\Tests\Phone;

use Vanat\SymDocUtils\Phone\PhoneNumberUtils;
use PHPUnit\Framework\TestCase;

class PhoneNumberUtilsTest extends TestCase
{

    public function testGetUniquePhoneNumbers()
    {
        $phoneNumbers = ["+420 736 755 875", "00420736755875", "+420736755875", "736755875", "+421789654123", "00421 789654123"];
        dump(implode(",",PhoneNumberUtils::getUniquePhoneNumbers($phoneNumbers)));
    }

    public function test_phoneNumMutation() {
        dump(PhoneNumberUtils::mutatePhoneNumber("736 755 875"));
        dump(PhoneNumberUtils::mutatePhoneNumber("+420 736 755 875"));
        dump(PhoneNumberUtils::mutatePhoneNumber("00420 736 755 875"));
        dump(PhoneNumberUtils::mutatePhoneNumber("004210736755875"));
        dump(PhoneNumberUtils::mutatePhoneNumber("+421736755875"));
    }
}
