<?php


namespace App\Tests\Symfony\Output;


use PHPUnit\Framework\TestCase;
use Vanat\SymDocUtils\Symfony\Output\EchoOutput;

class EchoOutputTest extends TestCase
{

    public function test_echo() {
        $output = new EchoOutput();
        $output->writeln("Text 1");
        $output->writeln("Text 2");
    }

}