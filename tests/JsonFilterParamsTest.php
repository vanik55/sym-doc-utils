<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 11.12.2018
 * Time: 10:38
 */

namespace App\Tests;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use Vanat\SymDocUtils\Doctrine\JsonFilterParameters;
use Vanat\SymDocUtils\Testing\BaseTestCase;
use Zend\EventManager\EventManagerInterface;

class JsonFilterParamsTest extends TestCase
{

    public function test_addDateFilters() {

        $json = '{"b.additionalData.area_name||a.data||o.elmo": "Praha"}';
        $jfp = JsonFilterParameters::createFromJsonParameters($json);
        dump($jfp->getParams());
        dump($jfp->getExpandedColumns("b.additionalData.area_name"));
    }

}
