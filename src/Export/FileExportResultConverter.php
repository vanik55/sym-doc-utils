<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 08.01.2019
 * Time: 10:03
 */

namespace Vanat\SymDocUtils\Export;


interface FileExportResultConverter
{

    function convert(FileExportResult $fileExportResult);

}