<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 08.01.2019
 * Time: 9:34
 */

namespace Vanat\SymDocUtils\Export;

class BinaryResponseFileExportConverter implements FileExportResultConverter
{

    function convert(FileExportResult $fileExportResult)
    {
        return FileExportUtils::createResponse($fileExportResult);
    }
}