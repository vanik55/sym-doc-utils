<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 08.01.2019
 * Time: 9:14
 */

namespace Vanat\SymDocUtils\Export;


interface FileExporter
{

    function export(\Iterator $iterator): FileExportResult;

}