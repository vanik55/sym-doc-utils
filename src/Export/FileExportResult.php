<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 08.01.2019
 * Time: 9:14
 */

namespace Vanat\SymDocUtils\Export;


class FileExportResult
{

    private $temporaryFileName;

    private $realFileName;

    private $mimeType;

    /**
     * FileExportResult constructor.
     * @param $temporaryFileName
     * @param $realFileName
     * @param $mimeType
     */
    public function __construct($temporaryFileName, $realFileName, $mimeType = "application/octet-stream")
    {
        $this->temporaryFileName = $temporaryFileName;
        $this->realFileName = $realFileName;
        $this->mimeType = $mimeType;
    }

    /**
     * @return mixed
     */
    public function getTemporaryFileName()
    {
        return $this->temporaryFileName;
    }

    /**
     * @return mixed
     */
    public function getRealFileName()
    {
        return $this->realFileName;
    }

    /**
     * @return string
     */
    public function getMimeType(): string
    {
        return $this->mimeType;
    }



}