<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 08.01.2019
 * Time: 9:22
 */

namespace Vanat\SymDocUtils\Export;


use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class FileExportUtils
{

    public static function createResponse(FileExportResult $fileExportResult): Response {

        $response = new BinaryFileResponse($fileExportResult->getTemporaryFileName());
        $response->headers->set("Content-Type", $fileExportResult->getMimeType());
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $fileExportResult->getRealFileName());
        return $response;

    }

}