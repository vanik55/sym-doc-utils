<?php

namespace Vanat\SymDocUtils\Mapper;

use Vanat\SymDocUtils\Doctrine\CustomPaginator;
use Vanat\SymDocUtils\Doctrine\GridMapper;

abstract class AbstractObjectMapper implements GridMapper
{

    function createDto($object) {
        return $this->fillObject($object);
    }

    abstract function fillObject($sourceObject, $fillObject = null);

    public function createDtoArray($objects):array {
        $array = [];
        foreach ($objects as $object) {
            $array[] = $this->createDto($object);
        }
        return $array;
    }

    function map(\Iterator $iterator, CustomPaginator $customPaginator): array
    {
        return $this->createDtoArray($iterator);
    }

}
