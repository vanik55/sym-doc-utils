<?php


namespace Vanat\SymDocUtils\Mapper;


use Vanat\SymDocUtils\Doctrine\CustomPaginator;
use Vanat\SymDocUtils\Doctrine\GridMapper;

/**
 * @deprecated
 */
abstract class AbstractMapper implements GridMapper
{

    abstract function createDto($object);

    public function createDtoArray($objects):array {
        $array = [];
        foreach ($objects as $object) {
            $array[] = $this->createDto($object);
        }
        return $array;
    }

    function map(\Iterator $iterator, CustomPaginator $customPaginator): array
    {
        return $this->createDtoArray($iterator);
    }

}
