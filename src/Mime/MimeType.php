<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 09.01.2019
 * Time: 9:35
 */

namespace Vanat\SymDocUtils\Mime;


class MimeType
{
    const XLSX_MIME_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    const XML_MIME_TYPE = "application/xml";
}