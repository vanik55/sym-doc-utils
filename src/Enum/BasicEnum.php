<?php

namespace Vanat\SymDocUtils\Enum;


use ReflectionClass;

abstract class BasicEnum
{
    private static $constCacheArray = NULL;

    private static function getConstants()
    {
        if (self::$constCacheArray == NULL) {
            self::$constCacheArray = [];
        }
        $calledClass = get_called_class();
        if (!array_key_exists($calledClass, self::$constCacheArray)) {
            $reflect = new ReflectionClass($calledClass);
            self::$constCacheArray[$calledClass] = $reflect->getConstants();
        }
        return self::$constCacheArray[$calledClass];
    }

    public static function isValidName($name, $strict = false): bool
    {
        $constants = self::getConstants();

        if ($strict) {
            return array_key_exists($name, $constants);
        }

        $keys = array_map('strtolower', array_keys($constants));
        return in_array(strtolower($name), $keys);
    }

    public static function isValidValue($value, $strict = true): bool
    {
        $values = array_values(self::getConstants());
        return in_array($value, $values, $strict);
    }

    public static function valueOf($name) {
        $constants = self::getConstants();
        if (array_key_exists($name, $constants)) {
            return $constants[$name];
        }
        return FALSE;
    }

    public static function constantNameOf($value) {
        $constants = self::getConstants();
        $keys = array_keys($constants);
        $values = array_values($constants);
        $index = array_search($value, $values);
        if ($index !== FALSE) {
            return $keys[$index];
        }
        return FALSE;
    }

}
