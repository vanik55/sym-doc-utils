<?php


namespace Vanat\SymDocUtils\Json;


class JsonUtils
{

    public static function isJson($possibleJson) {
        json_decode($possibleJson);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * @param string $possibleJson
     * @return string|array|null
     */
    public static function convertValueToStringOrArray(?string $possibleJson) {
        if(self::isJson($possibleJson)) {
            return json_decode($possibleJson, true);
        } else {
            return $possibleJson;
        }
    }

}