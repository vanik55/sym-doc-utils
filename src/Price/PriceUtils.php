<?php


namespace Vanat\SymDocUtils\Price;


class PriceUtils
{

    /**
     * Výpočet ceny po slevě
     * @param $basePrice
     * @param $discount
     * @return float
     */
    public static function afterDiscountPrice($basePrice, $discount): float
    {
        return $basePrice - ($basePrice * $discount * 0.01);
    }

}