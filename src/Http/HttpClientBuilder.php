<?php

namespace Vanat\SymDocUtils\Http;

use GuzzleHttp\Client;

class HttpClientBuilder
{

    public static function createHttpClient(array $headers = [], int $timeout = 30, int $connectTimeout = 15): Client
    {
        return new Client(["headers" => $headers,
            "timeout" => $timeout,
            "connect_timeout" => $connectTimeout
        ]);
    }

    public static function createDefaultHttpClient():Client  {
        return self::createHttpClient();
    }

}
