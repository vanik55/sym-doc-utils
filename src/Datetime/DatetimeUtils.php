<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 11.12.2018
 * Time: 9:20
 */

namespace Vanat\SymDocUtils\Datetime;


use DateInterval;
use DatePeriod;
use DateTime;
use RuntimeException;

class DatetimeUtils
{

    const DATETIME = 'DATETIME';
    const DATE = 'DATE';

    const DAY_NAMES = ["Pondělí","Úterý","Středa","Čtvrtek","Pátek","Sobota","Neděle"];

    public static function parseDate(string $dateString): DateTime {
        return DateTime::createFromFormat("!Y-m-d", $dateString);
    }

    public static function parseDateTime(string $dateTimeString, $trunc = false): DateTime {
        $datetime = DateTime::createFromFormat("Y-m-d\TH:i:s", $dateTimeString);
        if($datetime === FALSE) {
            $datetime = DateTime::createFromFormat("Y-m-d\TH:i:s.u\Z", $dateTimeString);
        }
        if($trunc) {
            $datetime->setTime(0, 0, 0, 0);
        }
        return $datetime;
    }

    public static function convertDateTimeToDbFormat(DateTime $dateTime):string {
        return $dateTime->format("Y-m-d H:i:s");
    }

    public static function detectDateFormat(string $string) {
        return strpos($string, 'T') !== false ? self::DATETIME : self::DATE;
    }

    /**
     * @param string $temporal
     * @param bool $trunc
     * @return string
     */
    public static function convertTemporalStringToDbString(string $temporal, bool $trunc = false): string {
        $datetime = self::detectDateFormat($temporal) === self::DATE ? self::parseDate($temporal) : self::parseDateTime($temporal, $trunc);
        return self::convertDateTimeToDbFormat($datetime);
    }

    public static function truncToDate(DateTime $dateTime): ?DateTime {
        if($dateTime) {
            $d = clone $dateTime;
            $d->setTime(0,0,0,0);
            return $d;
        }
        return null;
    }

    public static function getFirstDayOfMonth(?DateTime $dateTime = null): DateTime {
        if(!$dateTime) {
            $dateTime = new DateTime("first day of this month");
            $dateTime->setTime(0,0,0,0);
            return $dateTime;
        }
        $d = clone $dateTime;
        $d->setTime(0,0,0,0);
        $d->modify("first day of this month");
        return $d;
    }

    public static function getLastDayOfMonth(?DateTime $dateTime = null): DateTime {
        if(!$dateTime) {
            $dateTime = new DateTime("last day of this month");
            $dateTime->setTime(0,0,0,0);
            return $dateTime;
        }
        $d = clone $dateTime;
        $d->setTime(0,0,0,0);
        $d->modify("last day of this month");
        return $d;
    }

    /**
     * @param DateTime $from
     * @param DateTime $till
     * @return array
     */
    public static function getFirstDaysOfMonthsBetweenDates(DateTime $from, DateTime $till) {
        $result = array();
        $actual = clone $from;
        do {
            $actual->modify("first day of this month");
            $result[] = clone $actual;
            $actual->modify("+ 1 month");
        } while ($actual <= $till);
        return $result;
    }

    public static function generateDateInterval(DateTime $from, DateTime $till) {
        $tillCopy = clone $till;
        $tillCopy->setTime(0,0,1);

        $period = new DatePeriod(
            $from,
            new DateInterval("P1D"),
            $tillCopy
        );

        $result = [];

        $today = new DateTime();
        $today->setTime(0,0,0);

        foreach ($period as $day) {
            $t = 0;
            if($day != $today) {
                $t = $day < $today ? -1 : 1;
            }
            $result[] = array("date" => $day, "weekend_date" => self::isWeekendDay($day), "today" => $t);
        }

        return $result;
    }

    private static function isWeekendDay(DateTime $day) {
        return $day->format("N") >= 6;
    }

    public static function translateDateToName(DateTime $dateTime, bool $strip = false) {
        $name = self::DAY_NAMES[$dateTime->format("N")-1];
        return $strip ? mb_substr($name,0,2) : $name;
    }

    public static function createPeriod(DateTime $from, DateTime $till, bool $includeLastDay = true) {
        $tillCopy = clone $till;
        if($includeLastDay) {
            $tillCopy->setTime(0,0,1);
        }
        return new DatePeriod(
            $from,
            new DateInterval("P1D"),
            $tillCopy
        );
    }

    /**
     * Vygenerování měsíců mezi dvěma zadaným daty (za každé měsíc je v poli jedno datum -> první den daného měsíce)
     * @param DateTime $from
     * @param DateTime $till
     * @return array|DateTime[]
     */
    public static function getMonthsBetween(DateTime $from, DateTime $till) {
        $start    = (clone $from)->modify('first day of this month');
        $end      = (clone $till)->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period   = new DatePeriod($start, $interval, $end);

        $result = [];

        foreach ($period as $dt) {
            $result[] = $dt;
        }

        return $result;
    }

    /**
     * Vygenerování roků mezi dvěma zadaným daty (za každý rok je v poli jedno datum -> první den daného roku)
     * @param DateTime $from
     * @param DateTime $till
     * @return array|DateTime[]
     */
    public static function getYearsBetween(DateTime $from, DateTime $till) {
        $start    = (clone $from)->modify('first day of january this year');
        $end      = (clone $till)->modify('first day of january next year');
        $interval = DateInterval::createFromDateString('1 year');
        $period   = new DatePeriod($start, $interval, $end);

        $result = [];

        foreach ($period as $dt) {
            $result[] = $dt;
        }

        return $result;
    }

    /**
     * @param string|null $modifyString
     * @return DateTime
     */
    public static function createDateTime(?string $modifyString = null):?DateTime {
        try {
            if($modifyString) {
                return (new DateTime())->modify($modifyString);
            }
            return new DateTime();
        } catch (\Exception $e) {
            throw new RuntimeException("Datum nelze vytvořit!");
        }
    }

    /**ł
     * Bezpečná (dolní i horní mez moho být null) verze zjištění zdali je datum mezi dvěma mezními datumy
     * @param DateTime $dateTime testované datum
     * @param DateTime|null $from dolní mez (null => nekonečně v minulosti)
     * @param DateTime|null $till horní mez (null => nekonečně v budoucnosti)
     * @return bool
     */
    public static function isDateTimeBetween(DateTime $dateTime, ?DateTime $from, ?DateTime $till):bool {
        return (is_null($from)||$from <= $dateTime) && (is_null($till)||$till >= $dateTime);
    }

    public static function add(DateTime $dateTime, int $addValue, string $type = 'day'):DateTime {
        $newDate = clone $dateTime;
        $newDate->modify($addValue." $type");
        return $newDate;
    }

    public static function getDayNumber(DateTime $dateTime, int $offset = 0):int {
        return $dateTime->format("N") - 1 + $offset;
    }


    public static function generateTimesBetween(DateTime $start, DateTime $end, int $interval, string $intervalType = "minute"):array {
        $result = [clone $start];
        $newDate = $start;
        while (true) {
            $newDate = self::add($newDate, $interval, $intervalType);
            if($newDate > $end) {
                return $result;
            }
            $result[] = clone $newDate;
        }
    }

    public static function mergeDateAndTime(DateTime $date, string $hour):DateTime {
        return new DateTime($date->format("Y-m-d")." ".$hour);
    }

    /**
     * @param \DateTime $dateTime1
     * @param \DateTime $dateTime2
     * @return int
     */
    public static function getMinutesBetween(DateTime $dateTime1, DateTime $dateTime2):int {
        $sinceStart = $dateTime1->diff($dateTime2);
        $minutes = $sinceStart->days * 24 * 60;
        $minutes += $sinceStart->h * 60;
        $minutes += $sinceStart->i;
        return $dateTime1 > $dateTime2 ?  -$minutes : $minutes;
    }

    /**
     * @param \DateTime $dateTime1
     * @param \DateTime $dateTime2
     * @return int
     */
    public static function getSecondsBetween(DateTime $dateTime1, DateTime $dateTime2):int {
        $sinceStart = $dateTime1->diff($dateTime2);
        $seconds = $sinceStart->days * 24 * 60 * 60;
        $seconds += $sinceStart->h * 60 * 60;
        $seconds += $sinceStart->i * 60;
        $seconds += $sinceStart->s;
        return $dateTime1 > $dateTime2 ?  -$seconds : $seconds;
    }

    /**
     * @param array|DateTime[] $dates
     * @return array|string[]
     */
    public static function dateArrayToInClause(array $dates):array {
        $result = [];
        foreach ($dates as $date) {
            $result[] = $date->format("Y-m-d");
        }
        return $result;
    }

    public static function getDaysShortcutsFromBitmap(int $daysBitMap): array {
        $result = [];
        foreach (self::DAY_NAMES as $index => $dayName) {
            if($daysBitMap & pow(2, $index)) {
                $result[] = mb_substr($dayName, 0, 2);
            }
        }
        return $result;
    }

}
