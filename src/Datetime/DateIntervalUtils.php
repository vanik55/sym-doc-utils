<?php

namespace Vanat\SymDocUtils\Datetime;


class DateIntervalUtils
{

    /**
     * @param string $interval řetězec ve formátu HH:MM:SS nebo MM:SS
     * @return \DateInterval
     */
    public static function parseTime(string $interval): \DateInterval
    {
        try {
            return new \DateInterval("PT" . TimeUtils::parseTime($interval) . "S");
        } catch (\Exception $e) {
            throw new \InvalidArgumentException("Invalid interval: " . $interval);
        }
    }

    /**
     * @param \DateTime $from
     * @param \DateTime $till
     * @param int $daysBitmap
     * @return array|\DatePeriod[]
     */
    public static function createPeriods(\DateTime $from, \DateTime $till, int $daysBitmap): array
    {

        // Vrozmezí $from a $till vytvoří pole (spojité) DatePeriodů, které obsahují pouze dny, které jsou nastaveny v $daysBitmap

        $periods = [];
        $actualPeriod = [];
        // cyklus od $from do $till
        $current = clone $from;
        while ($current <= $till) {

            // pokud je nastavený den v bitmapě, vytvoříme nový DatePeriod
            if ($daysBitmap & (1 << ($current->format("N") - 1))) {
                $actualPeriod[] = clone $current;
            } else if ($actualPeriod) {
                $period = new \DatePeriod($actualPeriod[0], new \DateInterval("P1D"), $actualPeriod[count($actualPeriod) - 1]);
                $periods[] = $period;
                $actualPeriod = [];
            }

            // posuneme se o jeden den
            $current->modify("+1 day");
        }

        if ($actualPeriod) {
            $periods[] = new \DatePeriod($actualPeriod[0], new \DateInterval("P1D"), $actualPeriod[count($actualPeriod) - 1]);
        }

        return $periods;

    }

    public static function emptyInterval(): \DateInterval
    {
        return new \DateInterval("PT0S");
    }

}
