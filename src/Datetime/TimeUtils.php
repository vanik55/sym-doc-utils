<?php


namespace Vanat\SymDocUtils\Datetime;


class TimeUtils
{

    /**
     * Parsing času ve formatu HH:MM:SS (nebo MM:SS) do sekundového intervalu
     * @param string $timeString
     * @return int
     */
    public static function parseTime(string $timeString): int
    {
        if (substr_count($timeString, ":") == 2) {
            return strtotime($timeString) - strtotime("TODAY");
        } else {
            return strtotime("00:" . $timeString) - strtotime("TODAY");
        }
    }

    /**
     * Ověření, zdali je časová složka parametru $dateTime mezi parametry timeFrom a timeTill
     * @param string|null $timeFrom
     * @param string|null $timeTill
     * @param \DateTime|null $dateTime
     * @return bool
     */
    public static function isTimeBetween(?string $timeFrom, ?string $timeTill, ?\DateTime $dateTime): bool
    {
        $dateTime = $dateTime ?? new \DateTime();
        $absolute = strtotime($dateTime->format("H:i:s"));

        return (empty($timeFrom) || strtotime($timeFrom) <= $absolute)
            && (empty($timeTill) || strtotime($timeTill) >= $absolute);
    }

    /**
     * Konverze času vyjadřeného v sekundách do formátu MM:SS, pokud je počet hodin > 0, pak je formát HH:MM:SS
     * @param int $seconds
     * @return string
     */
    public static function secondsToTime(int $seconds): string
    {
        $hours = floor($seconds / 3600);
        $mins = floor($seconds / 60 % 60);
        $secs = floor($seconds % 60);
        if ($hours > 0) {
            return sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
        } else {
            return sprintf('%02d:%02d', $mins, $secs);
        }
    }

    /**
     * Konverze času vyjadřeného v minutách do formátu HH:MM (hodinová složka neni omezena 24 hodinami)
     * @param int $seconds
     * @return string
     */
    public static function minutesToTime(int $minutes): string
    {
        $hours = floor($minutes / 60);
        $mins = $minutes % 60;
        if ($hours > 0) {
            return sprintf('%02d:%02d', $hours, $mins);
        } else {
            return sprintf('00:%02d', $mins);
        }
    }

}