<?php

namespace Vanat\SymDocUtils\Datetime;

class DateTimeConverter
{
    public static function mutableFromInterface(\DateTimeInterface $dateTimeInterface): \DateTime
    {
        return new \DateTime('@'.$dateTimeInterface->getTimestamp(), $dateTimeInterface->getTimezone());
    }
}
