<?php


namespace Vanat\SymDocUtils\Report;


use DateTime;
use InvalidArgumentException;
use Vanat\SymDocUtils\Datetime\DatetimeUtils;

class PivotedReportUtils
{

    public static function generateDateTimedReportColumns(DateTime $from, DateTime $till, string $groupType,
                                                          $defaultRowValue = 0): PivotedReportColumns
    {
        if ($groupType == 'ALL') {
            return new PivotedReportColumns([["key" => "ALL", "header" => "Celkem"]], $defaultRowValue);
        } else if ($groupType == 'DAY') {
            setlocale( LC_TIME, 'cs_CZ');
            $period = DatetimeUtils::createPeriod($from, $till);
            $days = [];
            /** @var DateTime $day */
            foreach ($period as $day) {
                $days[] = ["key" => $day->format("Y-m-d"), "header" => DatetimeUtils::translateDateToName($day, true)." ".$day->format("d.n.y")];
            }
            return new PivotedReportColumns($days, $defaultRowValue);
        } else if ($groupType == 'MONTH') {
            $monthsPeriod = DatetimeUtils::getMonthsBetween($from, $till);
            $months = [];
            foreach ($monthsPeriod as $month) {
                if(!key_exists($month->format("n"),$months)) {
                    $months[$month->format("n")] = ["key" => $month->format("n"), "header" => $month->format("n")];
                }
            }
            return new PivotedReportColumns($months, $defaultRowValue);
        } else if ($groupType == 'YEAR') {
            $yearsPeriod = DatetimeUtils::getYearsBetween($from, $till);
            $years = [];
            foreach ($yearsPeriod as $year) {
                $years[] = ["key" => $year->format("Y"), "header" => $year->format("Y")];
            }
            return new PivotedReportColumns($years, $defaultRowValue);
        }
        throw new InvalidArgumentException("Požadovaný typ sekupení pivotovaného reportu neexistuje");
    }

    public static function calculateReportsDates(string $type, string $from, string $till): ReportDateInterval
    {
        if ($type == 'ALL' || $type == 'DAY') {
            return new ReportDateInterval(new DateTime($from), new DateTime($till));
        } else if ($type == 'MONTH') {
            return new ReportDateInterval(
                new DateTime($from."-01"),
                DatetimeUtils::getLastDayOfMonth(new DateTime($till."-01"))
            );
        } else if ($type == 'YEAR') {
            return new ReportDateInterval(
                new DateTime($from."-01-01"),
                new DateTime($till."-12-31")
            );
        }
    }

}
