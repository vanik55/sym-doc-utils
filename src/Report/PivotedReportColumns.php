<?php


namespace Vanat\SymDocUtils\Report;


class PivotedReportColumns
{

    /**
     * @var array
     */
    private $columns = [];

    /**
     * @var array
     */
    private $defaultRow = [];

    /**
     * PivotedReportColumns constructor.
     * @param array $columns
     * @param int $defaultRowValue
     */
    public function __construct(array $columns, $defaultRowValue = 0)
    {
        foreach ($columns as $value) {
            $this->columns[] = $value["header"];
            $this->defaultRow[] = [$value["key"] => $defaultRowValue];
        }
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @return array
     */
    public function getDefaultRow(): array
    {
        return $this->defaultRow;
    }

    public function mergeRow(array $row)
    {
        $result = [];
        $defaults = $this->defaultRow;
        foreach ($row as $entry) {
            $entryKey = key($entry);
            foreach ($defaults as &$dRow) {
                $dRowKey = key($dRow);
                if($entryKey == $dRowKey) {
                    $dRow[$dRowKey] = $entry[$entryKey] ?? (float)0;;
                    break;
                }
            }
        }
        foreach ($defaults as $default) {
            $result[] = $default[key($default)];
        }
        return $result;
    }


}