<?php


namespace Vanat\SymDocUtils\Report;


use DateTime;

class ReportDateInterval
{

    /**
     * @var DateTime
     */
    private $from;

    /**
     * @var DateTime
     */
    private $till;

    /**
     * ReportDateInterval constructor.
     * @param DateTime $from
     * @param DateTime $till
     */
    public function __construct(DateTime $from, DateTime $till)
    {
        $this->from = $from;
        $this->till = $till;
    }

    /**
     * @return DateTime
     */
    public function getFrom(): DateTime
    {
        return $this->from;
    }

    /**
     * @return DateTime
     */
    public function getTill(): DateTime
    {
        return $this->till;
    }


}