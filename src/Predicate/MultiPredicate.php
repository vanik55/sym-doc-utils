<?php


namespace Vanat\SymDocUtils\Predicate;


abstract class MultiPredicate implements Predicate
{

    /**
     * @var array|\Vanat\SymDocUtils\Predicate\Predicate[]
     */
    private $predicates = [];

    function add(Predicate $predicate) {
        $this->predicates[] = $predicate;
    }

    protected function getPredicates():array {
        return $this->predicates;
    }

}