<?php


namespace Vanat\SymDocUtils\Predicate;


class MultiAndPredicate extends MultiPredicate
{

    function test(Testable $testable): bool
    {
        foreach ($this->getPredicates() as $predicate) {
            if(!$predicate->test($testable)) {
                return false;
            }
        }
        return true;
    }
}