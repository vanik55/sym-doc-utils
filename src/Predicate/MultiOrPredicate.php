<?php


namespace Vanat\SymDocUtils\Predicate;


class MultiOrPredicate extends MultiPredicate {


    function test(Testable $testable): bool
    {
        foreach ($this->getPredicates() as $predicate) {
            if($predicate->test($testable)) {
                return true;
            }
        }
        return false;
    }
}