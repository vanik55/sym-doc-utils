<?php


namespace Vanat\SymDocUtils\Predicate;


class NegatePredicate implements Predicate
{

    /**
     * @var \Vanat\SymDocUtils\Predicate\Predicate
     */
    private $predicate;

    public function __construct(Predicate $predicate)
    {

        $this->predicate = $predicate;
    }

    function test(Testable $testable): bool
    {
        return !$this->predicate->test($testable);
    }
}