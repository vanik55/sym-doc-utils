<?php


namespace Vanat\SymDocUtils\Predicate;


interface Predicate
{
    function test(Testable $testable):bool;
}