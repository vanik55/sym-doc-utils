<?php

namespace Vanat\SymDocUtils\Stream;

class StreamUtils
{

    /**
     * Convert a string into a stream resource
     *
     * @param string $string The string to convert
     *
     * @return resource A stream resource.
     */
    function fromString(string $string)
    {
        $stream = fopen('php://memory','r+');
        fwrite($stream, $string);
        rewind($stream);
        return $stream;
    }

}
