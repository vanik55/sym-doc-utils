<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 23.01.2019
 * Time: 9:15
 */

namespace Vanat\SymDocUtils\Validation;


class ValidationResult
{
    const OK = "OK";
    const ERROR = "ERROR";

    /**
     * @var string
     *
     */
    public $globalStatus;

    /**
     * @var string
     */
    public $globalMessage;

    /**
     * @var array|FieldValidationResult[]|null
     */
    public $fieldsValidationResults;

    public function __construct()
    {
        $this->fieldsValidationResults = array();
    }

    public function selfEvaluate(?string $errorMessage = null, ?string $okMessage = null)
    {
        foreach ($this->fieldsValidationResults as $result) {
            if ($result->getFieldValidationStatus() == self::ERROR) {
                $this->setResult(self::ERROR, $errorMessage ?? "Validace selhala!");
                return;
            }
        }
        $this->setResult(self::OK, $okMessage ?? "Validace v pořádku.");
    }

    public function setResult($globalStatus, $globalMessage): ValidationResult
    {
        $this->globalStatus = $globalStatus;
        $this->globalMessage = $globalMessage;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGlobalStatus()
    {
        return $this->globalStatus;
    }

    /**
     * @param mixed $globalStatus
     * @return ValidationResult
     */
    public function setGlobalStatus($globalStatus)
    {
        $this->globalStatus = $globalStatus;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGlobalMessage()
    {
        return $this->globalMessage;
    }

    /**
     * @param mixed $globalMessage
     * @return ValidationResult
     */
    public function setGlobalMessage($globalMessage)
    {
        $this->globalMessage = $globalMessage;
        return $this;
    }

    /**
     * @return null
     */
    public function getFieldsValidationResults()
    {
        return $this->fieldsValidationResults;
    }

    /**
     * @param null $fieldsValidationResults
     * @return ValidationResult
     */
    public function setFieldsValidationResults($fieldsValidationResults)
    {
        $this->fieldsValidationResults = $fieldsValidationResults;
        return $this;
    }

    public function addFieldValidationResult(string $fieldValidationStatus, string $fieldCode, string $fieldName, string $fieldValidationMessage)
    {
        $this->fieldsValidationResults[] = new FieldValidationResult($fieldValidationStatus, $fieldCode, $fieldName, $fieldValidationMessage);
    }

    public function addFieldValidationOK(string $fieldCode, string $fieldName, string $fieldValidationMessage)
    {
        $this->fieldsValidationResults[] = new FieldValidationResult(ValidationResult::OK, $fieldCode, $fieldName, $fieldValidationMessage);
    }


    public function addFieldValidationError(string $fieldCode, string $fieldName, string $fieldValidationMessage)
    {
        $this->fieldsValidationResults[] = new FieldValidationResult(ValidationResult::ERROR, $fieldCode, $fieldName, $fieldValidationMessage);
    }

}