<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 23.01.2019
 * Time: 9:34
 */

namespace Vanat\SymDocUtils\Validation;


class ValidationException extends \Exception
{

    /**
     * @var ValidationResult
     */
    private $validationResult;

    /**
     * ValidationException constructor.
     * @param ValidationResult $validationResult
     * @param string|null $message
     */
    public function __construct(ValidationResult $validationResult, ?string $message = null)
    {
        $this->validationResult = $validationResult;
        if($message) {
            $this->message = $message;
        }
    }

    /**
     * @return ValidationResult
     */
    public function getValidationResult(): ValidationResult
    {
        return $this->validationResult;
    }


}