<?php


namespace Vanat\SymDocUtils\Validation;


class EmailValidator
{

    public static function isEmailValid(string $email):bool {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        return true;
    }

}