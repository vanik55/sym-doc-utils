<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 23.01.2019
 * Time: 9:33
 */

namespace Vanat\SymDocUtils\Validation;


interface Validatable
{

    function validate(): ValidationResult;

}