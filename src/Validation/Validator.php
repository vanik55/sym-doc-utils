<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 23.01.2019
 * Time: 9:36
 */

namespace Vanat\SymDocUtils\Validation;


class Validator
{

    protected $globalOkMessage;
    protected $globalErrorMessage;

    /**
     * Validator constructor.
     * @param $globalOkMessage
     * @param $globalErrorMessage
     */
    public function __construct(?string $globalOkMessage = null, ?string $globalErrorMessage = null)
    {
        $this->globalOkMessage = $globalOkMessage;
        $this->globalErrorMessage = $globalErrorMessage;
    }


    /**
     * @param Validatable $validatable
     * @return ValidationResult
     */
    public function validate(Validatable $validatable): ValidationResult {
        $result = $validatable->validate();
        if(!$result->getGlobalStatus()) {
            $result->selfEvaluate($this->globalErrorMessage, $this->globalOkMessage);
        }
        return $result;
    }

    /**
     * @param Validatable $validatable
     * @return ValidationResult
     * @throws ValidationException
     */
    public function validateAndThrows(Validatable $validatable): ValidationResult {
        $result =  $this->validate($validatable);
        if($result->getGlobalStatus() != ValidationResult::OK) {
            throw new ValidationException($result, "Validace objektu selhala. Viz detaily.");
        }
        return $result;
    }

}