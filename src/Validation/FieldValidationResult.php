<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 23.01.2019
 * Time: 9:16
 */

namespace Vanat\SymDocUtils\Validation;


class FieldValidationResult
{

    /**
     * @var string
     */
    public $fieldValidationStatus;

    /**
     * @var string
     */
    public $fieldCode;

    /**
     * @var string
     */
    public $fieldName;

    /**
     * @var string
     */
    public $fieldValidationMessage;


    /**
     * FieldValidationResult constructor.
     * @param $fieldValidationStatus
     * @param $fieldCode
     * @param $fieldName
     * @param $fieldValidationMessage
     */
    public function __construct(string $fieldValidationStatus, string $fieldCode, string $fieldName, string $fieldValidationMessage)
    {
        $this->fieldValidationStatus = $fieldValidationStatus;
        $this->fieldCode = $fieldCode;
        $this->fieldName = $fieldName;
        $this->fieldValidationMessage = $fieldValidationMessage;
    }

    /**
     * @return string
     */
    public function getFieldValidationStatus(): string
    {
        return $this->fieldValidationStatus;
    }

    /**
     * @param string $fieldValidationStatus
     * @return FieldValidationResult
     */
    public function setFieldValidationStatus(string $fieldValidationStatus): FieldValidationResult
    {
        $this->fieldValidationStatus = $fieldValidationStatus;
        return $this;
    }

    /**
     * @return string
     */
    public function getFieldCode(): string
    {
        return $this->fieldCode;
    }

    /**
     * @param string $fieldCode
     * @return FieldValidationResult
     */
    public function setFieldCode(string $fieldCode): FieldValidationResult
    {
        $this->fieldCode = $fieldCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getFieldName(): string
    {
        return $this->fieldName;
    }

    /**
     * @param string $fieldName
     * @return FieldValidationResult
     */
    public function setFieldName(string $fieldName): FieldValidationResult
    {
        $this->fieldName = $fieldName;
        return $this;
    }

    /**
     * @return string
     */
    public function getFieldValidationMessage(): string
    {
        return $this->fieldValidationMessage;
    }

    /**
     * @param string $fieldValidationMessage
     * @return FieldValidationResult
     */
    public function setFieldValidationMessage(string $fieldValidationMessage): FieldValidationResult
    {
        $this->fieldValidationMessage = $fieldValidationMessage;
        return $this;
    }



}