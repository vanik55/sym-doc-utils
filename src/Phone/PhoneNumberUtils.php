<?php


namespace Vanat\SymDocUtils\Phone;


use Vanat\SymDocUtils\String\StringUtils;

class PhoneNumberUtils
{

    public static function getUniquePhoneNumbers(array $phoneNumbers):array {
        $result = [];
        foreach ($phoneNumbers as $phoneNumber) {
            $phoneNumber = preg_replace('/\s+/', '', $phoneNumber);
            $phoneNumber = preg_replace('/^00420/', '', $phoneNumber);
            $phoneNumber = preg_replace('/^\+420/', '', $phoneNumber);
            $phoneNumber = preg_replace('/^00/', '+', $phoneNumber);
            $result[] = $phoneNumber;
        }
        return array_unique($result);
    }

    const CZECH_PHONE_PREFIX_PLUS = "+420";
    const CZECH_PHONE_PREFIX_DOUBLE_ZERO = "00420";

    /**
     * @param string $originPhoneNumber
     * @return array
     */
    public static function mutatePhoneNumber(string $originPhoneNumber):array {
        $trimmedNum = preg_replace('/\s+/', '', $originPhoneNumber);

        $result = [$trimmedNum];

        if(!StringUtils::startsWith($trimmedNum, "+")
            && !StringUtils::startsWith($trimmedNum, "00")) {
            $result[] = self::CZECH_PHONE_PREFIX_DOUBLE_ZERO.$trimmedNum;
            $result[] = self::CZECH_PHONE_PREFIX_PLUS.$trimmedNum;
        } elseif (StringUtils::startsWith($trimmedNum, self::CZECH_PHONE_PREFIX_PLUS)) {
            $result[] = ltrim($trimmedNum, self::CZECH_PHONE_PREFIX_PLUS);
            $result[] = self::CZECH_PHONE_PREFIX_DOUBLE_ZERO.ltrim($trimmedNum, self::CZECH_PHONE_PREFIX_PLUS);
        } elseif (StringUtils::startsWith($trimmedNum, self::CZECH_PHONE_PREFIX_DOUBLE_ZERO)) {
            $result[] = ltrim($trimmedNum, self::CZECH_PHONE_PREFIX_DOUBLE_ZERO);
            $result[] = self::CZECH_PHONE_PREFIX_PLUS.ltrim($trimmedNum, self::CZECH_PHONE_PREFIX_DOUBLE_ZERO);
        } elseif (StringUtils::startsWith($trimmedNum, "+")) {
            $result[] = "00".ltrim($trimmedNum, "+");
        } elseif (StringUtils::startsWith($trimmedNum, "00")) {
            $result[] = "+".ltrim($trimmedNum, "00");
        }

        return $result;

    }

}
