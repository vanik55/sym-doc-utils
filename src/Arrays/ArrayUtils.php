<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 11.12.2018
 * Time: 9:59
 */

namespace Vanat\SymDocUtils\Arrays;


class ArrayUtils
{

    public static function isAssoc(array $arr)
    {
        if (array() === $arr) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    public static function getValueFromAssoc($key, array $assocArray, $notFoundValue = null) {
        if(array_key_exists($key, $assocArray)) {
            return $assocArray[$key];
        } else {
            return $notFoundValue;
        }
    }

}