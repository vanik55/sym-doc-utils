<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 20.12.2018
 * Time: 15:22
 */

namespace Vanat\SymDocUtils\String;


use Transliterator;

class StringUtils
{

    public static function getInitials(?string $firstName, ?string $lastName)
    {
        $result = "";
        if ($firstName) {
            $result .= strtoupper(mb_substr($firstName,0,1));
        }
        if ($lastName) {
            $result .= strtoupper(mb_substr($lastName, 0, 1));
        }
        return $result;
    }

    public static function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    public static function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    public static function removeDiacritics($string) {
        $transliterator = Transliterator::createFromRules(':: Any-Latin; :: Latin-ASCII; :: NFD; :: [:Nonspacing Mark:] Remove; ::  NFC;', Transliterator::FORWARD);
        return $normalized = $transliterator->transliterate($string);
    }

    public static function nullIf($testString, $condition) {
        return $testString == $condition ? NULL : $testString;
    }

    public static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Všechna první písmena ve slovech převede na velká písmena
     * @param string|null $str
     * @return string|null
     */
    public static function toUpperCaseTitle(?string $str):?string {
        if(is_null($str)) {
            return null;
        }
        return mb_convert_case($str, MB_CASE_TITLE, 'UTF-8');
    }

    public static function removeAllWhitespace(string $string) {
        return preg_replace('/\s+/', '', $string);
    }

    public static function coalesce() {
        $args = func_get_args();
        foreach ($args as $arg) {
            if (!empty($arg)) {
                return $arg;
            }
        }
        return NULL;
    }

}
