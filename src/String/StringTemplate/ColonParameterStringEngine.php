<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 12.03.2019
 * Time: 12:49
 */

namespace Vanat\SymDocUtils\String\StringTemplate;


class ColonParameterStringEngine extends Engine
{

    public function __construct()
    {
        parent::__construct(':', '');
    }

}