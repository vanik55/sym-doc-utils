<?php

namespace Vanat\SymDocUtils\Numeric;

class PercentageUtils
{

    /**
     * @param $old
     * @param $new
     * @param int $precision
     * @return float
     */
    public static function pctChange($old, $new, int $precision = 2): float
    {
        if ($old == 0) {
            $old++;
            $new++;
        }

        $change = (($new - $old) / $old) * 100;

        return round($change, $precision);
    }

}
