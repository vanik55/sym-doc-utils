<?php


namespace Vanat\SymDocUtils\Numeric;


class NumericUtils
{

    public static function sumOfDigits(int $num):int {
        $sum=0; $rem=0;
        for ($i =0; $i<=strlen($num);$i++)
        {
            $rem=$num%10;
            $sum = $sum + $rem;
            $num=$num/10;
        }
        return $sum;
    }

    public static function isNumberBetween($number, $lowerLimit, $upperLimit):bool {
        return ($number >= $lowerLimit || is_null($lowerLimit)) && ($number <= $upperLimit || is_null($upperLimit));
    }

    public static function sign($n): int
    {
        return $n == 0 ? 1 : $n <=> 0;
    }

}
