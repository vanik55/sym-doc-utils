<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 08.01.2019
 * Time: 12:41
 */

namespace Vanat\SymDocUtils\Excel;


use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * Sekvenční generátor excel souboru
 * Class SequentialExcelBuilder
 * @package Vanat\SymDocUtils\Excel
 */
class SequentialExcelBuilder
{

    /**
     * @var Spreadsheet
     */
    private $spreadsheet;

    /**
     * @var \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet
     */
    private $activeSheet;

    /**
     * @var int
     */
    private $row = 1;

    /**
     * @var int
     */
    private $column = 1;

    private $numOfCols = 0;

    public function __construct(?array $header = null, ?string $sheetName = null)
    {
        $this->spreadsheet = new Spreadsheet();
        $this->activeSheet = $this->spreadsheet->getActiveSheet();
        if ($sheetName) {
            $this->activeSheet->setTitle($sheetName);
        }
        $this->row = $this->column = 1;
        if ($header) {
            $this->writeHeader($header);
        }
    }

    public function writeHeader(array $header)
    {
        $this->numOfCols = count($header);
        $this->activeSheet->getStyleByColumnAndRow($this->column, $this->row, $this->numOfCols, $this->row)
            ->getFont()->setBold(true)->setSize(12);
        foreach ($header as $columnName) {
            $this->activeSheet->getColumnDimensionByColumn($this->column)->setAutoSize(true);
            $this->writeString($columnName);
        }
        $this->nextRow();
    }

    public function writeString($value, $multiline = false)
    {
        if (is_null($value)) {
            return $this->writeNull();
        }
        if ($multiline) {
            $this->activeSheet->getStyleByColumnAndRow($this->column, $this->row)->getAlignment()->setWrapText(true);
        }
        $this->activeSheet->setCellValueExplicitByColumnAndRow($this->column++, $this->row, $value, DataType::TYPE_STRING);
        return $this;
    }

    public function writeNull()
    {
        $this->activeSheet->setCellValueExplicitByColumnAndRow($this->column++, $this->row, null, DataType::TYPE_NULL);
        return $this;
    }

    public function nextRow()
    {
        $this->row++;
        $this->column = 1;
        return $this;
    }

    public function writeNumeric($value)
    {
        if (is_null($value)) {
            return $this->writeNull();
        }
        $this->activeSheet->setCellValueExplicitByColumnAndRow($this->column++, $this->row, $value, DataType::TYPE_NUMERIC);
        return $this;
    }

    public function writeDateTime(?\DateTime $dateTime)
    {
        if (is_null($dateTime)) {
            return $this->writeNull();
        }
        $this->activeSheet->setCellValueExplicitByColumnAndRow($this->column, $this->row, Date::PHPToExcel($dateTime), DataType::TYPE_NUMERIC);
        $this->activeSheet->getStyleByColumnAndRow($this->column, $this->row)->getNumberFormat()->setFormatCode("d. m. yyyy hh:mm:ss");
        $this->column++;
        return $this;
    }

    public function writeDate(?\DateTime $dateTime)
    {
        if (is_null($dateTime)) {
            return $this->writeNull();
        }
        $this->activeSheet->setCellValueExplicitByColumnAndRow($this->column, $this->row, Date::PHPToExcel($dateTime), DataType::TYPE_NUMERIC);
        $this->activeSheet->getStyleByColumnAndRow($this->column, $this->row)->getNumberFormat()->setFormatCode("d. m. yyyy");
        $this->column++;
        return $this;
    }

    public function makeLastRowBold() {
        $this->activeSheet->getStyleByColumnAndRow($this->column, $this->row - 1, $this->numOfCols, $this->row - 1)
            ->getFont()->setBold(true)->setSize(12);
    }

    public function makeLastColumnBold() {
        $this->activeSheet->getStyleByColumnAndRow($this->numOfCols, 0, $this->numOfCols, $this->row)
            ->getFont()->setBold(true)->setSize(12);
    }

    public function createFile($customName = null)
    {
        $tmpFileName = $customName ?? (uniqid() . "xlsx");
        $tmpFile = tempnam(sys_get_temp_dir(), $tmpFileName);

        $xlsWriter = new Xlsx($this->spreadsheet);
        $xlsWriter->save($tmpFile);

        return $tmpFile;
    }

}
