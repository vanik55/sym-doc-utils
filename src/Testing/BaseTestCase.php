<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 30.10.2018
 * Time: 23:09
 */

namespace Vanat\SymDocUtils\Testing;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BaseTestCase extends KernelTestCase
{

    protected $mykernel;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    public function setUp() {
        self::bootKernel();
        $this->mykernel = static::$kernel;
        $this->em = static::$kernel->getContainer()->get('doctrine')->getManager();
    }

    /**
     *
     * @param string $name
     * @return \Doctrine\Common\Persistence\ObjectRepository|\Doctrine\ORM\EntityRepository
     */
    protected function getRepository($name) {
        return $this->em->getRepository($name);
    }

    protected function storeAndFlush($entity) {
        $this->em->persist($entity);
        $this->em->flush();
    }

    protected function mergeAndFlush($entity) {
        $this->em->merge($entity);
        $this->em->flush();
    }

    /**
     * @param $className
     * @param $id
     * @return null|object
     */
    protected function getEntity($className, $id) {
        return $this->em->find($className, $id);
    }

    /**
     * @param $serviceName
     * @return mixed
     */
    protected function getService($serviceName) {
        return $this->mykernel->getContainer()->get($serviceName);
    }

    /**
     *
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection() {
        return $this->em->getConnection();
    }


    protected function serializeToJSON($forSerialization) {
        return $this->getService("jms_serializer")->serialize($forSerialization,'json');
    }

    protected function serializeToJSONAndPrint($forSerialization) {
        echo PHP_EOL.$this->serializeToJSON($forSerialization);
    }

    protected function dumpEntity($entity, $maxDepth = 2) {
        \Doctrine\Common\Util\Debug::dump($entity, $maxDepth);
    }

}