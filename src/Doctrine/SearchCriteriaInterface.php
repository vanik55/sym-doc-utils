<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 30.11.2018
 * Time: 8:07
 */

namespace Vanat\SymDocUtils\Doctrine;


use Doctrine\ORM\QueryBuilder;

interface SearchCriteriaInterface
{
    function addToQueryBuilder(QueryBuilder $qb);
}