<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 10.12.2018
 * Time: 12:25
 */

namespace Vanat\SymDocUtils\Doctrine;


class JsonFilterParameters extends FilterParameters
{

    public static function createFromJsonParameters(?string $json, array $paramNameTranslate = []): JsonFilterParameters
    {
        if(empty($json)) {
            $json = '{}';
        }
        $params = json_decode($json, true);
        $fp = new JsonFilterParameters();
        foreach ($params as $key => $value) {
            $keyParts = explode("||", $key);
            if (count($keyParts) > 1) {
                $fp->addExpandedColumn($keyParts[0], array_slice($keyParts, 1));
            }
            $key = $keyParts[0];
            if(isset($paramNameTranslate[$key])) {
                $fp->addParameter($paramNameTranslate[$key], $value);
            } else {
                $fp->addParameter($key, $value);
            }
        }
        return $fp;
    }

}
