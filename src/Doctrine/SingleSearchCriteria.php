<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 30.11.2018
 * Time: 8:09
 */

namespace Vanat\SymDocUtils\Doctrine;


use Doctrine\ORM\QueryBuilder;

class SingleSearchCriteria implements SearchCriteriaInterface
{

    private $expression;

    public function __construct(string $whereQuery)
    {
        $this->expression = $whereQuery;
    }

    function addToQueryBuilder(QueryBuilder $qb)
    {
        $qb->andWhere($this->expression);
    }
}