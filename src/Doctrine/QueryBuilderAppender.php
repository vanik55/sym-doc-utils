<?php


namespace Vanat\SymDocUtils\Doctrine;


use Doctrine\ORM\QueryBuilder;

class QueryBuilderAppender
{

    /**
     * @var \Doctrine\ORM\QueryBuilder
     */
    private $queryBuilder;

    public function __construct(QueryBuilder $queryBuilder)
    {
        $this->queryBuilder = $queryBuilder;
    }

    public function appendLeftJoin(string $joinClause, string $alias, ?string $conditionType = null,
                                    ?string $condition = null) {
        if(!in_array($alias, $this->queryBuilder->getAllAliases())) {
            $this->queryBuilder->leftJoin($joinClause, $alias, $conditionType, $condition);
        }
    }

    public function appendJoin(string $joinClause, string $alias, ?string $conditionType = null,
                               ?string $condition = null) {
        if(!in_array($alias, $this->queryBuilder->getAllAliases())) {
            $this->queryBuilder->join($joinClause, $alias, $conditionType, $condition);
        }
    }

}