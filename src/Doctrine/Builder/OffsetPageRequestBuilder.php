<?php

namespace Vanat\SymDocUtils\Doctrine\Builder;

use FOS\RestBundle\Request\ParamFetcherInterface;
use Vanat\SymDocUtils\Doctrine\OffsetPageRequest;

class OffsetPageRequestBuilder
{

    public static function createFromParamFetcher(ParamFetcherInterface $params): OffsetPageRequest
    {
        return new OffsetPageRequest($params->get("start") - 1, $params->get("limit"));
    }

}
