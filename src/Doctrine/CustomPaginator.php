<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 30.10.2018
 * Time: 14:04
 */

namespace Vanat\SymDocUtils\Doctrine;


use Doctrine\ORM\Tools\Pagination\Paginator;
use InvalidArgumentException;
use Vanat\SymDocUtils\Export\FileExporter;
use Vanat\SymDocUtils\Export\FileExportResult;
use Vanat\SymDocUtils\Export\FileExportResultConverter;

class CustomPaginator extends Paginator
{

    private $withOneExtraRow;

    private $metadata;

    /**
     *
     * @param \Doctrine\ORM\Query $query
     * @param Boolean $fetchJoinCollection
     * @param object|array|null $metadata
     * @param bool $withOneExtraRow
     * @param bool $useOutputWalkers
     */
    function __construct($query, $fetchJoinCollection = true, $metadata = null, $withOneExtraRow = false, $useOutputWalkers = false) {
        parent::__construct($query, $fetchJoinCollection);
        //$this->setUseOutputWalkers(false);
        $this->withOneExtraRow = $withOneExtraRow;
        $this->metadata = $metadata;
        $this->setUseOutputWalkers($useOutputWalkers);
    }

    /**
     * @param $query
     * @param bool $fetchJoinCollection
     * @param $metadata
     * @param bool $withOneExtraRow
     * @return \Vanat\SymDocUtils\Doctrine\CustomPaginator
     */
    public static function withOutputWalkers($query, bool $fetchJoinCollection = true, $metadata = null, bool $withOneExtraRow = false): CustomPaginator
    {
        return new CustomPaginator($query, $fetchJoinCollection, $metadata, $withOneExtraRow, true);
    }

    /**
     * Počet záznamů celkem
     * @return integer
     */
    function getTotalEntries() {
        return $this->count();
    }

    /**
     * Celkový počet stránek
     * @return integer
     */
    function getPageCount() {
        return ceil($this->count()/($this->getMaxResults()));
    }

    /**
     * Číslo aktuální stránky
     * @return integer
     */
    function getActualPage() {
        return ($this->getQuery()->getFirstResult()/$this->getMaxResults())+1;
    }

    /**
     * Maximální počet záznamů na stránku
     * @return integer
     */
    function getMaxResults() {
        return $this->withOneExtraRow ? ($this->getQuery()->getMaxResults() - 1) : $this->getQuery()->getMaxResults();
    }

    /**
     * Počet záznamů na aktuální stránce
     * @return type
     */
    function getActualPageSize() {
        return ($this->withOneExtraRow and !$this->isLastPage())? max(array(0,$this->getIterator()->count() - 1))  : $this->getIterator()->count();
    }

    function getWithOneExtraRow() {
        return $this->withOneExtraRow;
    }

    function isLastPage() {
        return ($this->getActualPage() == $this->getPageCount()) ? true : false;
    }

    public function getMetadata() {
        return $this->metadata;
    }

    public function addMetadata(string $key, $value) {
        if(is_null($this->metadata)) {
            $this->metadata = [];
        }
        $this->metadata[$key] = $value;
    }

    /**
     * @param GridMapper|null $mapper
     * @return array
     */
    function getSerializedResult(GridMapper $mapper = null) {
        if($this->getUseOutputWalkers()) {
            $this->getQuery()->setHint("knp_paginator.count", $this->count());
        }

        if($this->withOneExtraRow and is_null($mapper)) {
            throw new InvalidArgumentException("Nelze použít funkci extra řádku resultsetu bez vlastního mapperu!!!");
        }
        try {
            $data = is_null($mapper) ? $this->getIterator()->getArrayCopy() : $mapper->map($this->getIterator(), $this);
        } catch (\Exception $e) {
            throw new InvalidArgumentException($e->getMessage(), $e->getCode(), $e);
        }
        return array(
            "totalEntries" => $this->getTotalEntries(),
            "actualPage" => $this->getActualPage(),
            "actualPageSize" => count($data),
            "maxResults" => $this->getMaxResults(),
            "pageCount" => $this->getPageCount(),
            "data" => $data,
            "metadata" => $this->metadata
        );
    }


    /**
     * @param FileExporter $fileExporter
     * @param FileExportResultConverter|null $fileExportResultConverter
     * @return mixed
     */
    function exportResultToFile(FileExporter $fileExporter, ?FileExportResultConverter $fileExportResultConverter = null) {
        return $fileExportResultConverter ? $fileExportResultConverter->convert($fileExporter->export($this->getIterator()))
            : $fileExporter->export($this->getIterator());
    }

}
