<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 30.10.2018
 * Time: 22:57
 */

namespace Vanat\SymDocUtils\Doctrine;


class PageRequest
{

    const ASC_DIRECTION = 'ASC';
    const DESC_DIRECTION = 'DESC';
    const DEFAULT_PAGE_NUMBER = 1;
    const DEFAULT_PAGE_SIZE = 50;

    private $pageNumber;
    private $pageSize;
    private $orderColumn;
    private $orderDirection;

    function __construct($orderColumn = null, $orderDirection = self::ASC_DIRECTION, $pageNumber = self::DEFAULT_PAGE_NUMBER, $pageSize = self::DEFAULT_PAGE_SIZE)
    {
        $this->pageNumber = $pageNumber;
        $this->pageSize = $pageSize;
        $this->orderColumn = $orderColumn;
        $this->orderDirection = $orderDirection;
    }

    /**
     *
     * @param \FOS\RestBundle\Request\ParamFetcherInterface $params
     * @param bool $forExport
     * @return PageRequest
     */
    public static function createFromParamFetcher(\FOS\RestBundle\Request\ParamFetcherInterface $params, bool $forExport = FALSE): PageRequest
    {
        return new PageRequest($params->get("sort"), $params->get("direction"),
            $forExport ? 1 : $params->get("page"), $forExport ? 100000 : $params->get("pageSize"));
    }

    public function translateOrderColumn(array $translateArray):PageRequest {
        if($this->orderColumn && $translateArray) {
            $this->orderColumn = $translateArray[$this->orderColumn] ?? $this->orderColumn;
        }
        return $this;
    }

    function getPageNumber()
    {
        return $this->pageNumber;
    }

    function setPageNumber($pageNumber)
    {
        $this->pageNumber = $pageNumber;
    }

    function getPageSize()
    {
        return $this->pageSize;
    }

    function setPageSize($pageSize)
    {
        $this->pageSize = $pageSize;
    }

    function getOrderDirection()
    {
        return $this->orderDirection;
    }

    function setOrderDirection($orderDirection)
    {
        $this->orderDirection = $orderDirection;
    }

    function appendToQuery(\Doctrine\ORM\QueryBuilder $qb, $addExtraRow = false)
    {
        $qb->setMaxResults($addExtraRow ? $this->pageSize + 1 : $this->pageSize);
        $qb->setFirstResult(($this->pageNumber - 1) * $this->pageSize);
        if ($this->orderColumn) {
            foreach (explode(",", $this->getOrderColumn()) as $column) {
                if (substr_count($column, ".") > 1) {
                    $parts = explode(".", $column, 3);
                    $qb->addOrderBy(sprintf("JSON_EXTRACT(%s, '$.%s')", $parts[0].".".$parts[1], $parts[2]), $this->orderDirection);
                } else {
                    $qb->addOrderBy($column, $this->orderDirection);
                }
            }
        }
    }

    function getOrderColumn()
    {
        return $this->orderColumn;
    }

    function setOrderColumn($orderColumn)
    {
        $this->orderColumn = $orderColumn;
    }

}
