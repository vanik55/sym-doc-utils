<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 30.10.2018
 * Time: 23:00
 */

namespace Vanat\SymDocUtils\Doctrine;


use Doctrine\ORM\QueryBuilder;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Vanat\SymDocUtils\Arrays\ArrayUtils;
use Vanat\SymDocUtils\Datetime\DatetimeUtils;

class FilterParameters implements \Iterator
{

    private $params = array();

    /**
     * V daných slopupcích se bude vyhledávat na rovnost '=', místo defaultního LIKE
     * @var array
     */
    private $strictColumns = array();

    private $index;

    private $expandedColumn = array();

    /**
     *
     * @param ParamFetcherInterface $params
     * @param string $prefix
     * @return FilterParameters
     */
    public static function createFromParamFetcher(ParamFetcherInterface $params, string $prefix = "filter"): FilterParameters
    {
        $fp = new FilterParameters();
        if ($params->get($prefix)) {
            foreach ($params->get($prefix) as $key => $value) {
                $fp->addParameter($key, $value);
            }
        }
        return $fp;
    }

    public function addParameter($key, $value)
    {
        $this->params[$key] = $value;
    }

    public function setExpandColumns($expandColumn)
    {
        $this->expandedColumn = $expandColumn;
    }

    public function addExpandedColumn(string $name, $expander)
    {
        $this->expandedColumn[$name] = $expander;
    }

    public function containsOneOfKeys(array $keys): bool
    {
        foreach ($keys as $key) {
            if ($this->containsKey($key)) {
                return true;
            }
        }
        return false;
    }

    public function containsKey($key): bool
    {
        return array_key_exists($key, $this->params);
    }

    public function setStrictColumns(array $strictColumns)
    {
        return $this->strictColumns = $strictColumns;
    }

    /**
     * Vrátí hodnotu asociovanou s daným klíčem a vymaže daný klíč s pole parametrů, pokud klíč neexistuje, vrátí null
     * @param $key
     * @return mixed
     */
    public function tryPopParameter($key)
    {
        if ($this->containsKey($key)) {
            return $this->popParameter($key);
        }
        return null;
    }

    /**
     * Vrátí hodnotu asociovanou s daným klíčem a vymaže daný klíč s pole parametrů
     * @param $key
     * @return mixed
     */
    public function popParameter($key)
    {
        $value = $this->getParameter($key);
        $this->removeKey($key);
        return $value;
    }

    public function getParameter($key)
    {
        return $this->params[$key];
    }

    public function removeKey($key)
    {
        //TODO: Resetovat index či ne?
        unset($this->params[$key]);
    }

    public function current()
    {
        return $this->params[$this->index];
    }

    public function next()
    {
        $this->index++;
    }

    public function valid()
    {
        return isset($this->params[$this->key()]);
    }

    public function key()
    {
        return $this->index;
    }

    public function reverse()
    {
        $this->params = array_reverse($this->params);
        $this->rewind();
    }

    public function rewind()
    {
        $this->index = 0;
    }

    public function addWhereClause(QueryBuilder $qb, $fixedPredicate = "1=1", array $expandedColumns = null)
    {
        if ($expandedColumns) {
            $this->expandedColumn = $expandedColumns;
        }
        $qb->andWhere($qb->expr()->andX($fixedPredicate, $this->getConditions($qb)));
    }

    private function getConditions(QueryBuilder $qb)
    {
        $and = $qb->expr()->andX();
        foreach ($this->getParams() as $key => $value) {
            if (is_array($value)) {
                if (ArrayUtils::isAssoc($value)) {
                    $from = $key;
                    if (key_exists("from", $value) && !is_null($value["from"])) {
                        $from = is_numeric($value["from"]) ? $value["from"] : "'" . DatetimeUtils::convertTemporalStringToDbString($value["from"]) . "'";
                    }
                    $truncDate = false;
                    $to = $key;
                    if (key_exists("to", $value) && !is_null($value["to"])) {
                        if (is_numeric($value["to"])) {
                            $to = $value["to"];
                        } else {
                            $to = "'" . DatetimeUtils::convertTemporalStringToDbString($value["to"]) . "'";
                            $truncDate = DatetimeUtils::detectDateFormat($value["to"]) == DatetimeUtils::DATE;
                        }
                    }

                    $and->add($qb->expr()->between($truncDate ? sprintf("CAST(%s AS DATE)", $key) : $key, $from, $to));

                } else {
                    if (count($value) > 0) {
                        $and->add($qb->expr()->in($key, $value));
                    }
                }
            } else if (substr_count($key, ".") > 1) {
                // rozděl klíč na části po druhé tečce
                $parts = explode(".", $key, 3);
                $and->add(sprintf("JSON_EXTRACT(%s, '$.%s') LIKE '%%%s%%'", $parts[0] . "." . $parts[1], $parts[2], $value));
            } else {
                $cols = $this->getExpandedColumns($key);
                $orX = $qb->expr()->orX();
                foreach ($cols as $col) {
                    $orX->add(in_array($key, $this->strictColumns)
                        ? $qb->expr()->eq($col, "'" . trim($value) . "'")
                        : $qb->expr()->like($col, "'%" . trim($value) . "%'"));
                }
                $and->add($orX);
            }
        }
        return $and;
    }

    function getParams()
    {
        return $this->params;
    }

    public function getExpandedColumns(string $name): array
    {
        $result = [$name];
        if (array_key_exists($name, $this->expandedColumn)) {
            $expander = $this->expandedColumn[$name];
            if (is_array($expander)) {
                foreach ($expander as $e) {
                    $result[] = $e;
                }
            } else {
                $result[] = $expander;
            }
        }
        return $result;
    }

    public function addHavingClause(QueryBuilder $qb, $fixedPredicate = "1=1", array $expandedColumns = null)
    {
        if ($expandedColumns) {
            $this->expandedColumn = $expandedColumns;
        }
        $qb->andHaving($qb->expr()->andX($fixedPredicate, $this->getConditions($qb)));
    }

}
