<?php


namespace Vanat\SymDocUtils\Doctrine;


use FOS\RestBundle\Request\ParamFetcherInterface;

class ParamFetcherGridPageRequest extends GridPageRequest
{

    public function __construct(ParamFetcherInterface $pfi, bool $exportRequest = false, string $filterAttributeName = "filter")
    {
        $filterParameters = JsonFilterParameters::createFromJsonParameters($pfi->get($filterAttributeName));
        $pageRequest = PageRequest::createFromParamFetcher($pfi, $exportRequest);
        parent::__construct($filterParameters, $pageRequest);
    }

}
