<?php


namespace Vanat\SymDocUtils\Doctrine;


use Doctrine\DBAL\Connection;

class LockUtils
{

    public static function lockTable(Connection $connection, string $scheme, string $table, string $mode = "READ") {
        $connection->exec(sprintf("LOCK TABLES %s.%s %s", $scheme, $table, $mode));
    }

    public static function unlockTables(Connection $connection) {
        $connection->exec("UNLOCK TABLES");
    }

}