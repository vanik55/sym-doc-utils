<?php


namespace Vanat\SymDocUtils\Doctrine;



use Doctrine\ORM\QueryBuilder;

abstract class GridPageRequest
{

    /**
     * @var \Vanat\SymDocUtils\Doctrine\FilterParameters
     */
    private $filterParameters;
    /**
     * @var \Vanat\SymDocUtils\Doctrine\PageRequest
     */
    private $pageRequest;

    public function __construct(FilterParameters $filterParameters, PageRequest $pageRequest)
    {
        $this->filterParameters = $filterParameters;
        $this->pageRequest = $pageRequest;
    }

    public function mergeIntoQuery(QueryBuilder $queryBuilder, bool $useHavingInsteadOfWhere = false) {
        if ($useHavingInsteadOfWhere) {
            $this->filterParameters->addHavingClause($queryBuilder);
        } else {
            $this->filterParameters->addWhereClause($queryBuilder);
        }
        $this->pageRequest->appendToQuery($queryBuilder);
    }

    public function getFilterParameters():FilterParameters {
        return $this->filterParameters;
    }

}
