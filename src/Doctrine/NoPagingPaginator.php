<?php


namespace Vanat\SymDocUtils\Doctrine;


use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;
use InvalidArgumentException;

class NoPagingPaginator
{

    /**
     * @var \Doctrine\ORM\Query
     */
    private $query;

    /**
     * @var array|null
     */
    private $metadata;

    /**
     *
     * @param \Doctrine\ORM\Query $query
     * @param array|null $metadata
     */
    function __construct(\Doctrine\ORM\Query $query, ?array $metadata = null) {
        $this->metadata = $metadata;
        $this->query = $query;
    }

    /**
     * @param GridMapper|null $mapper
     * @return array
     */
    function getSerializedResult(GridMapper $mapper = null): array
    {
        try {
            $data = is_null($mapper) ? $this->query->getResult() :
                $mapper->map((new \ArrayObject($this->query->getResult()))->getIterator(), new CustomPaginator(null));
        } catch (\Exception $e) {
            throw new InvalidArgumentException($e->getMessage(), $e->getCode(), $e);
        }
        $count = count($data);
        return array(
            "totalEntries" => $count,
            "actualPage" => 1,
            "actualPageSize" => $count,
            "maxResults" => $count,
            "pageCount" => 1,
            "data" => $data,
            "metadata" => $this->metadata
        );
    }

}