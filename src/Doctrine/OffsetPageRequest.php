<?php

namespace Vanat\SymDocUtils\Doctrine;


use Doctrine\ORM\QueryBuilder;

class OffsetPageRequest
{

    /**
     * @var int
     */
    private $start;

    /**
     * @var int|null
     */
    private $limit;

    /**
     * @param int $start
     * @param int|null $limit
     */
    public function __construct(int $start = 0, ?int $limit = PHP_INT_MAX)
    {
        $this->start = $start;
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getStart(): int
    {
        return $this->start;
    }

    /**
     * @return int|null
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }

    public function appendToQueryBuilder(QueryBuilder $queryBuilder): QueryBuilder
    {
        return $queryBuilder->setFirstResult($this->start)->setMaxResults($this->limit);
    }


}
