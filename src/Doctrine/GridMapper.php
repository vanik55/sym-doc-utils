<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 30.10.2018
 * Time: 14:06
 */

namespace Vanat\SymDocUtils\Doctrine;


interface GridMapper
{
    function map(\Iterator $iterator, CustomPaginator $customPaginator);
}