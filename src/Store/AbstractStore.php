<?php


namespace Vanat\SymDocUtils\Store;


use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;

abstract class AbstractStore
{

    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param string $entityName
     * @param $id
     * @return object|mixed|null
     */
    public function getReference(string $entityName, $id) {
        try {
            return $this->em->getReference($entityName, $id);
        } catch (ORMException $e) {
            throw new \InvalidArgumentException(sprintf("Vyjimka %s pro tridu entity %s (id=%s): %s",
                get_class($e),
                $entityName,
                $id,
                $e->getMessage()));
        }
    }

    protected function persistAndFlush($entity) {
        $this->em->persist($entity);
        $this->em->flush();
        return $entity;
    }

}
