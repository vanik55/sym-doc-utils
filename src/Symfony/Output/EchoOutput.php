<?php


namespace Vanat\SymDocUtils\Symfony\Output;


use Symfony\Component\Console\Output\NullOutput;

class EchoOutput extends NullOutput
{

    public function write($messages, $newline = false, $options = self::OUTPUT_NORMAL)
    {
        echo $messages;
    }

    public function writeln($messages, $options = self::OUTPUT_NORMAL)
    {
        echo $messages.PHP_EOL;
    }

}