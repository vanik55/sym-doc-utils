<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 30.10.2018
 * Time: 14:07
 */

namespace Vanat\SymDocUtils\Symfony\Service;


use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\Driver\Statement;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BaseService implements ContainerAwareInterface
{

    protected $container;
    protected $logger;

    /**
     * BaseService constructor.
     * @param $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }


    /**
     * Sets the container.
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
        //$this->logger = $container->get("logger");
    }

    public function getProjectDir(?string $filePath = null): string
    {
        $projectDir = $this->container->getParameter("kernel.project_dir");
        return $filePath ? $projectDir . $filePath : $projectDir;
    }

    /**
     * @param $serviceName
     * @return mixed|object
     */
    protected function getService($serviceName)
    {
        return $this->container->get($serviceName);
    }

    /**
     * @param $entity
     */
    protected function storeAndFlush($entity)
    {
        try {
            $this->getEntityManager()->persist($entity);
            $this->getEntityManager()->flush();
        } catch (Exception $e) {
            throw new \InvalidArgumentException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     *
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        return $this->container->get('doctrine')->getManager();
    }

    /**
     * @param $entity
     * @return mixed
     * @throws Exception
     */
    protected function mergeAndFlush($entity)
    {
        try {
            $merged = $this->getEntityManager()->merge($entity);
            $this->getEntityManager()->flush();
            return $merged;
        } catch (Exception $e) {
            throw new \InvalidArgumentException($e->getMessage(), $e->getCode(), $e);
        }

    }

    /**
     * @param $entityName
     * @param $id
     * @return mixed
     */
    protected function getEntityReference($entityName, $id)
    {
        try {
            return $this->getEntityManager()->getReference($entityName, $id);
        } catch (ORMException $e) {
            throw new \InvalidArgumentException($e->getMessage(), $e->getCode(), $e);
        }
    }

    protected function flushDatabase()
    {
        try {
            $this->getEntityManager()->flush();
        } catch (OptimisticLockException|ORMException $e) {
            throw new \InvalidArgumentException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     *
     * @param string $entityName
     * @return EntityRepository|ObjectRepository|mixed
     */
    protected function getRepository($entityName)
    {
        return $this->getEntityManager()->getRepository($entityName);
    }

    protected function getParameter($paramName)
    {
        return $this->container->getParameter($paramName);
    }

    protected function prepareAndFetchAll($query, array $params = null)
    {
        $stmt = $this->prepareStatement($query, $params);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    /**
     * @param string $query
     * @param array|null $params
     * @return Statement
     */
    protected function prepareStatement($query, array $params = null)
    {
        $stmt = $this->getConnection()->prepare($query);
        if (!empty($params)) {
            foreach ($params as $key => $value) {
                $stmt->bindValue($key, $this->checkAndConvertIfNecessary($value));
            }
        }
        return $stmt;
    }

    /**
     *
     * @return Connection
     */
    protected function getConnection()
    {
        return $this->getEntityManager()->getConnection();
    }

    private function checkAndConvertIfNecessary($value)
    {
        if ($value instanceof \DateTime) {
            return $value->format("Y-m-d");
        }
        return $value;
    }

}