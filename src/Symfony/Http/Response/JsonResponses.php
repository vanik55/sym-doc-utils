<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 31.10.2018
 * Time: 11:43
 */

namespace Vanat\SymDocUtils\Symfony\Http\Response;


use Symfony\Component\HttpFoundation\JsonResponse;

class JsonResponses
{

    public static function generalOk(string $message = null, array $payload = null): JsonResponse
    {
        $result = array("status" => "success", "message" => $message ? $message : "OK");
        if ($payload) {
            $result["payload"] = $payload;
        }
        return new JsonResponse($result);
    }

    public static function badRequest(string $message = null, array $payload = null, int $httpCode = 400): JsonResponse
    {
        $result = array("status" => "error", "message" => $message ? $message : "Bad request");
        if ($payload) {
            $result["payload"] = $payload;
        }
        return new JsonResponse($result, $httpCode);
    }

    public static function badRequestWithCode(string $code, string $message = null, array $payload = null): JsonResponse
    {
        return self::createResponseWithCode($code, $message ? $message : "Bad Request", $payload, 400);
    }

    public static function conflictWithCode(string $code, string $message = null, array $payload = null): JsonResponse
    {
        return self::createResponseWithCode($code, $message ? $message : "Conflict", $payload, 409);
    }

    public static function createResponseWithCode(string $code, string $message = null, array $payload = null, int $httStatusCode = 0) {
        $result = array("code" => $code, "status" => "error", "message" => $message ? $message : "Conflict");
        if ($payload) {
            $result["payload"] = $payload;
        }
        return new JsonResponse($result, $httStatusCode);
    }

}