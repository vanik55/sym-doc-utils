<?php


namespace Vanat\SymDocUtils\Symfony\Http\Response;


use Symfony\Component\HttpFoundation\Response;

class Responses
{

    /**
     * @param string $name Jméno včetně extenze
     * @param $content
     * @return Response
     */
    public static function createOctetStreamResponse(string $name, $content):Response {

        $response = new Response();

        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', "application/octet-stream");
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $name . '";');
        $response->headers->set('Content-length', strlen($content));

        $response->setContent($content);
        $response->setCharset("UTF-8");

        return $response;

    }

}