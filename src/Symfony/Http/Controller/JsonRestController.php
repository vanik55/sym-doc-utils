<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 31.10.2018
 * Time: 9:31
 */

namespace Vanat\SymDocUtils\Symfony\Http\Controller;


use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerInterface;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class JsonRestController extends Controller
{

    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
        $trustedProxies = getenv("TRUSETD_PROXIES");
        $this->setTrustedProxies($trustedProxies ?? '127.0.0.1');
    }

    /**
     * @param string|null $trustedProxiesList
     */
    protected function setTrustedProxies(?string $trustedProxiesList = '127.0.0.1') {
        $trustedProxies = explode(",", $trustedProxiesList);
        Request::setTrustedProxies($trustedProxies, Request::HEADER_X_FORWARDED_ALL);
    }

    /**
     * @param $data
     * @param int $status
     * @param SerializationContext|null $context
     * @return JsonResponse
     */
    public function createJsonResponse($data, int $status = 200, ?SerializationContext $context = null) {
        return new JsonResponse($this->serializer->serialize($data, 'json', $context), $status, array(), true);
    }

    /**
     * @param $data
     * @param array $groups Serializační skupiny. NULL = pouze skupina Default
     * @param int $status
     * @return JsonResponse
     */
    public function createJsonResponseByGroups($data, ?array $groups = null, int $status = 200)
    {
        return new JsonResponse($this->serializer->serialize($data,
            'json',
            SerializationContext::create()->setGroups($groups ?? ["Default"])),
            $status, array(), true);
    }

    public function createSuccessResponse($message) {
        return $this->createResponse("success", $message);
    }

    public function createErrorResponse($message) {
        return $this->createResponse("error", $message);
    }

    public function createResponse($status, $message) {
        return new JsonResponse(array("status" => $status, "message" => $message));
    }

    public function getUser()
    {
        $user = parent::getUser();
        if(!$user) {
            $user = $this->getEntityManager()->find('Intranet\Core\Entity\User',getenv("FAKE_USER_ID") ?? 1);
        }
        return $user;
    }

    /**
     * @return ObjectManager|EntityManagerInterface
     */
    public function getEntityManager():ObjectManager {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @param $repository
     * @return ObjectRepository
     */
    public function getRepository($repository) {
        return $this->getDoctrine()->getRepository($repository);
    }

    public function flushDatabase() {
        $this->getEntityManager()->flush();
    }

    public function persistAndFlush($entity) {
        $this->getEntityManager()->persist($entity);
        $this->flushDatabase();
    }

    /**
     * @param $entity
     * @return object Merged entity
     */
    public function mergeAndFlush($entity) {
        $returnEntity = $this->getEntityManager()->merge($entity);
        $this->flushDatabase();
        return $returnEntity;
    }

    public function removeAndFlush($entity) {
        $this->getEntityManager()->remove($entity);
        $this->flushDatabase();
    }

    public function getSerializer():SerializerInterface {
        return $this->serializer;
    }

    public function getClientIP(){
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)){
            return  $_SERVER["HTTP_X_FORWARDED_FOR"];
        }else if (array_key_exists('REMOTE_ADDR', $_SERVER)) {
            return $_SERVER["REMOTE_ADDR"];
        }else if (array_key_exists('HTTP_CLIENT_IP', $_SERVER)) {
            return $_SERVER["HTTP_CLIENT_IP"];
        }
        return '';
    }

}
