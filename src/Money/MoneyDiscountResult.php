<?php

namespace Vanat\SymDocUtils\Money;

class MoneyDiscountResult
{

    /**
     * @var \Brick\Money\Money
     */
    private $amount;

    /**
     * @var float|null
     */
    private $totalDiscount;

    /**
     * @param \Brick\Money\Money $amount
     * @param float|null $totalDiscount
     */
    public function __construct(\Brick\Money\Money $amount, ?float $totalDiscount)
    {
        $this->amount = $amount;
        $this->totalDiscount = $totalDiscount;
    }

    /**
     * @return \Brick\Money\Money
     */
    public function getAmount(): \Brick\Money\Money
    {
        return $this->amount;
    }

    /**
     * @return float|null
     */
    public function getTotalDiscount(): ?float
    {
        return $this->totalDiscount;
    }

    /**
     * @return float
     */
    public function floatAmount(): float {
        return $this->amount->getAmount()->toFloat();
    }

}
