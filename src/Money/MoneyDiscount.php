<?php

namespace Vanat\SymDocUtils\Money;

use Brick\Math\RoundingMode;
use Brick\Money\Money;

class MoneyDiscount
{

    /**
     * @param \Brick\Money\Money $toDiscount Částka ke slevě
     * @param float|Money $discountValue Výše slevy v procentech (0-100%)
     * @return void
     */
    public static function applyDiscount(Money $toDiscount, $discountValue): Money {
        if ($discountValue instanceof Money) {
            $discount = $discountValue->dividedBy(100)->getAmount()->toFloat();
        } else {
            $discount = round($discountValue/100,2);
        }
        $_d = 1 - $discount;
        return $toDiscount->multipliedBy($_d, RoundingMode::HALF_UP);
    }

    /**
     * @param \Brick\Money\Money $toDiscount Částka ke slevě
     * @param array|float[] $discountValue pole slev (0-100%)
     * @return \Vanat\SymDocUtils\Money\MoneyDiscountResult
     */
    public static function applyMultipleDiscounts(Money $toDiscount, array $discountValue): MoneyDiscountResult
    {
        $totalDiscount = self::getTotalDiscount($discountValue) ?? 0;
        if ($totalDiscount >= 0) {
            $newPrice = $toDiscount->multipliedBy($totalDiscount, RoundingMode::HALF_UP);
            return new MoneyDiscountResult($newPrice, round(1 - $totalDiscount, 2)*100);
        } else {
            return new MoneyDiscountResult($toDiscount, null);
        }

    }

    /**
     * @param array $discountValue
     * @return float|null
     */
    private static function getTotalDiscount(array $discountValue): ?float
    {
        $totalDiscount = null;
        foreach ($discountValue as $value) {
            if ($value >= 0) {
                $value = 1 - round($value / 100, 2);
                $totalDiscount = ($totalDiscount ?? 1) * $value;
            }

        }
        return !is_null($totalDiscount) ? round($totalDiscount,2) : null;
    }

    /**
     * @param array $discountValues
     * @return float
     */
    public static function getTotalDiscountPct(array $discountValues): float {
        $totalDiscount = self::getTotalDiscount($discountValues);
        return !is_null($totalDiscount) ? round((1 - $totalDiscount)*100,2) : 0;
    }

}
