<?php

namespace Vanat\SymDocUtils\Money;

use Brick\Math\BigNumber;
use Brick\Math\RoundingMode;
use Brick\Money\Exception\UnknownCurrencyException;
use Brick\Money\Money;

class MoneyUtils
{

    /**
     * @param BigNumber|int|float|string $value
     * @param string $currency
     * @return \Brick\Money\Money
     */
    public static function of($value, string $currency = "CZK") {
        try {
            return Money::of($value, $currency, null, RoundingMode::HALF_UP);
        } catch (UnknownCurrencyException $e) {
            throw new \InvalidArgumentException($e->getMessage(), $e->getCode(), $e);
        }
    }

    public static function ceil(Money $value): Money {
        return self::of(ceil($value->getAmount()->toFloat()));
    }

    public static function addVatToPrice(Money $price, float $vat): Money {
        return self::of($price->getAmount()->toFloat() * (1 + $vat / 100));
    }

    public static function getVatFromBasePrice(Money $price, float $vat): Money {
        return self::of($price->getAmount()->toFloat() * ($vat / 100));
    }

}
