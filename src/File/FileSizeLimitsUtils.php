<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 01.04.2019
 * Time: 14:06
 */

namespace Vanat\SymDocUtils\File;


use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileSizeLimitsUtils
{

    /**
     * @param UploadedFile $uploadedFile
     * @throws UploadFileLimitExceedException
     */
    public static function validateUpload(?UploadedFile $uploadedFile) {
        if(!$uploadedFile) {
            throw new UploadFileLimitExceedException(sprintf("Neplataný soubor, nebo soubor překročil celkovou velikost požadavku serveru. (Limit požadavku %s B)"
                    ,self::parse_size(ini_get('post_max_size')))
                );
        }
        if(!$uploadedFile->getSize() or $uploadedFile->getSize() > self::file_upload_max_size()) {
            throw new UploadFileLimitExceedException(sprintf("Nahrávaný soubor překročil limity serveru pro jediný soubor. (soubor: %s B, limit %s B)",
                $uploadedFile->getSize() ?? "N/A", self::file_upload_max_size()));
        }
    }

    public static function file_upload_max_size() {
        static $max_size = -1;

        if ($max_size < 0) {
            // Start with post_max_size.
            $post_max_size = self::parse_size(ini_get('post_max_size'));
            if ($post_max_size > 0) {
                $max_size = $post_max_size;
            }

            // If upload_max_size is less, then reduce. Except if upload_max_size is
            // zero, which indicates no limit.
            $upload_max = self::parse_size(ini_get('upload_max_filesize'));
            if ($upload_max > 0 && $upload_max < $max_size) {
                $max_size = $upload_max;
            }
        }
        return $max_size;
    }

    private static function parse_size($size) {
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
        $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
        if ($unit) {
            // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
            return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
        }
        else {
            return round($size);
        }
    }



}